# Getting started

---

## PREPARE ENVIRONMENT

```Bash
  git clone git@bitbucket.org:whoerdev/browser-ext.git
  cd browser-ext
```
### install packages

```bash
    npm install
```

---

## Run build extension

To test and deploy the extension, you can run

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.

Extension is ready for testing and deployment!
