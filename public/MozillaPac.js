

let proxy_url = "DIRECT";

browser.runtime.onMessage.addListener((message, sender, sendResponse) => {
    if (
        message &&
        typeof message === 'object' &&
        message.type === 'setPacScriptProxy' &&
        message.payload &&
        typeof message.payload === 'string'
    ) {
        proxy_url = message.payload;
    }

    browser.runtime.sendMessage(proxy_url);
});

function FindProxyForURL(url, host) {
    switch (host) {
        case 'secwhapi.net':
        case 'test.whoer.net':
            return 'DIRECT';
        default:
            return proxy_url;
    }
}
