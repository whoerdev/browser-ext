
class Account {
    static validate(data) {
        if (!data || typeof data !== 'object') return;
        if (!data.code || typeof data.code !== 'string') return;
        if (!isFinite(data.created)) return;
        if (!isFinite(data.expires)) return;
        //TODO Plan is always number or not?
        // if (!isFinite(data.plan)) return;
        return true;
    }
}

export default Account;