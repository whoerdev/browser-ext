
class Proxy {
    static validate(data) {
        if (!data || typeof data !== 'object') return;
        if (!data.countryCode || typeof data.countryCode !== 'string') return;
        if (!data.countryName || typeof data.countryName !== 'string') return;
        if (!Array.isArray(data.servers)) return;
        if (data.servers.find(item => !Proxy.validateServer(item))) return;
        if (!data.trial && (!data.username || !data.password)) return;
        return true;
    }

    static validateServer(data) {
        if (!data || typeof data !== 'object') return;
        if (!data.host || !data.port) return;
        return true;
    }
}

export default Proxy;