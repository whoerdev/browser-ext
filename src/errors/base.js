
// AppError
initClass(AppError, Error);
export function AppError(message) {
    Error.call(this, message);

    this.name = 'AppError';
    this.message = message;

    // Error.captureStackTrace(this, this.constructor);
}

// ConnectError
initClass(ConnectError, AppError);
export function ConnectError(message) {
    Error.call(this, message);

    this.name = 'ConnectError';
    this.message = message;

    // Error.captureStackTrace(this, this.constructor);
}

// AccessError
initClass(AccessError, AppError);
export function AccessError(message) {
    Error.call(this, message);

    this.name = 'AccessError';
    this.message = message;

    // Error.captureStackTrace(this, this.constructor);
}

export function initClass(currClass, protoClass) {
    currClass.prototype = Object.create(protoClass.prototype);
    currClass.prototype.constructor = currClass;
}

export default AppError;