
import * as base from './base';
import * as api from './api';

export default {
    base,
    api
};