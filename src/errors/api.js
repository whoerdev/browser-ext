
import { AppError, initClass } from './base';

// ResponseError
initClass(AppError, ResponseError);
export function ResponseError(message) {
    Error.call(this, message);

    this.name = 'ResponseError';
    this.message = message;

    // Error.captureStackTrace(this, this.constructor);
}

// CodeInvalidError
initClass(AppError, CodeInvalidError);
export function CodeInvalidError(message) {
    Error.call(this, message);

    this.name = 'CodeInvalidError';
    this.message = message;

    // Error.captureStackTrace(this, this.constructor);
}

// CodeExpiredError
initClass(AppError, CodeExpiredError);
export function CodeExpiredError(message) {
    Error.call(this, message);

    this.name = 'CodeExpiredError';
    this.message = message;

    // Error.captureStackTrace(this, this.constructor);
}

// CodeLimitError
initClass(AppError, CodeLimitError);
export function CodeLimitError(message) {
    Error.call(this, message);

    this.name = 'CodeLimitError';
    this.message = message;

    // Error.captureStackTrace(this, this.constructor);
}
// EmailError
initClass(AppError, EmailError);
export function EmailError(message) {
    Error.call(this, message);

    this.name = 'EmailError';
    this.message = message;

    // Error.captureStackTrace(this, this.constructor);
}