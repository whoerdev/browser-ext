
const browser = window.browser || window.chrome;


const allCompetitorsVpnID = [
  'fgddmllnllkalaagkghckoinaemmogpe',
  'bfidboloedlamgdmenmlbipfnccokknp',
  'lblebdecfhdegbeoejplcpmhibbkbkin',
  'fjoaledfpmneenckfbpdfhkmimnjocfa',
  'ailoabdmgclmfmhdagmlohpjlbpffblp',
  'poeojclicodamonabcabmapamjkkmnnk',
  'hnmpcagpplmpfojmgmnngilcnanddlhb',
  'ffbkglfijbcbgblgflchnbphjdllaogb',
]

function detectCompetitor() {
  return getAllExtension().then(competitors =>
    competitors.filter(competitor => allCompetitorsVpnID.includes(competitor.id) && competitor.enabled)
    .map(competitor => {
      return {
        id: competitor.id,
        name: competitor.name,
        icon: competitor.icons[0].url,
      }
    })
  );
}

export function getAllExtension() {
  return new Promise((resolve, reject) => {
    try {
      browser.management.getAll(function(data) {
        const err = window.chrome.runtime.lastError;
        err ? reject(err) : resolve(data);
      });
    }
    catch (err) {
      reject(err);
    }
  });
}

function disabledAllExtension(arrExtensionsID) {
  arrExtensionsID.forEach(id => {
    // setEnabledExtension(id)
    //   .then()
    browser.management.setEnabled(id, false);
  })
}

export function setEnabledExtension(id, boolean = false) {
  return new Promise((resolve, reject) => {
    try {
      browser.management.setEnabled(id, boolean ,function(data) {
        const err = window.chrome.runtime.lastError;
        err ? reject(err) : resolve(data);
      });
    }
    catch (err) {
      reject(err);
    }
  });
}

export default {
  detectCompetitor,
  disabledAllExtension
}