
//const browser = window.browser || window.chrome;


export function sendMessage(data) {
    if (window.browser) {
        return window.browser.runtime.sendMessage(data, {}).catch(
            errorHandler
        );
    }

    return new Promise((resolve, reject) => {
        try {
            window.chrome.runtime.sendMessage(data, {}, function(data) {
                const err = window.chrome.runtime.lastError;
                err ? reject(err) : resolve(data);
            });
        }
        catch (err) {
            reject(err);
        }
    }).catch(
        errorHandler
    );

    function errorHandler(err) {
        console.log('Runtime message send failed', err);
    }
}

export function addListener(func) {
    const browser = window.browser || window.chrome;
    if(browser) {
        browser.runtime.onMessage.addListener(func);
    }
}

export function sendCustomMessage(event, payload, toProxy) {
    return sendMessage({
        type: event,
        payload
    });
}

export function addCustomListener(event, func) {
    return addListener((data, sender, sendResponse) => {
        if (data && typeof data === 'object' && data.type === event) {
            if (!func(data.payload, sender, sendResponse)) {
                sendResponse();
            }
        }
    });
}

export default {
    sendMessage,
    addListener,
    sendCustomMessage,
    addCustomListener
}