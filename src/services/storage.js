
const browser = window.browser || window.chrome;

export function addChangeListener(func) {
    browser.storage.onChanged.addListener(func);
}

export function getItems(keys) {
    if (window.browser) {
        return window.browser.storage.local.get(keys);
    }

    return new Promise((resolve, reject) => {
        try {
            window.chrome.storage.local.get(keys, function(data) {
                const err = window.chrome.runtime.lastError;
                err ? reject(err) : resolve(data);
            });
        }
        catch (err) {
            reject(err);
        }
    });
}

export function setItems(data) {
    if (window.browser) {
        return window.browser.storage.local.set(data);
    }

    return new Promise((resolve, reject) => {
        try {
            window.chrome.storage.local.set(data, function() {
                const err = window.chrome.runtime.lastError;
                err ? reject(err) : resolve();
            });
        }
        catch (err) {
            reject(err);
        }
    });
}

export function setItem(key, data) {
    return setItems({[key]: data});
}

export default {
    addChangeListener,
    getItems,
    setItems,
    setItem
}