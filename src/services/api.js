
import axios from 'axios';
// import uuid from "small-uuid";
import localeService from './locale';
import * as apiErrors from '../errors/api';
// import {EmailError} from "../errors/api";
// import {data} from "autoprefixer";


const BACKEND_BASE_URL = process.env.WHOER_BACKEND_BASE_URL;
const TEST_PROXY_ENABLED = process.env.WHOER_TEST_PROXY === 'true';
const URL_FOR_DETECT_IP = 'https://whoer.net/main/api/ip';


export function registerAccount(email) {
    // if (!email) email = uuid.create() + '@trial.com';

    return axios.post(`${getApiPath()}/register/proxy?email=${encodeURIComponent(email)}`).then(res => {
        const data = res.data;

        validateResponse(data);
        if (!data.code) throw new apiErrors.ResponseError('Code undefined');
        if (!data.expires) throw new apiErrors.ResponseError('Code expires undefined');

        return {
            email,
            code: data.code,
            expires: data.expires * 1000,
            plan: data.plan,
            trial: 1,
            created: (data.created * 1000) || Date.now()
        }
    });
}

export function getAccountInfo(code) {
    return axios.get(getApiPath() + '/info?code=' + code).then(res => {
        const data = res.data;

        validateResponse(data);
        if (!data.expires) throw new apiErrors.ResponseError('Code expires undefined');

        return {
            code,
            email: data.email,
            expires: data.expires * 1000,
            plan: data.plan,
            trial: data.trial,
            created: data.created * 1000
        }
    });
}

export function getTrialProxies(code) {
    return Promise.all([
        axios.get(getApiPath() + '/proxies?code=' + code).then(res => res.data),
        axios.get(getApiPath() + '/countries/proxy').then(res => res.data)
    ]).then(([proxiesData, countriesData]) => {
        validateResponse(proxiesData);
        validateResponse(countriesData);

        const list = Object.keys(countriesData.countries).map(countryCode => {
            const { auth } = proxiesData;
            const location = proxiesData.servers[countryCode];
            const servers = (location && location.hosts) || [];

            return {
                countryCode,
                countryName: countriesData.countries[countryCode],
                username: auth.username,
                password: auth.password,
                servers: servers,
                trial: !!servers.length
            };
        });

        if (TEST_PROXY_ENABLED) {
            list.push({
                countryCode: 'en',
                countryName: 'Test proxy',
                servers: [{host: "0.0.0.0", port: "8888"}],
                trial: 1
            })
        }

        return list;
    });
}

export function getIpInfo() {
    return axios.get(`${URL_FOR_DETECT_IP}`)
      .then(res => {
          const data = res.data;
          validateResponse(data);
          return {
              ip: data.data.ip,
              countryCode: data.data.iso || '-'
          }
      })
}

export function getPremiumProxies(code) {
    return axios.get(getApiPath() + '/proxies?code=' + code).then(res => res.data).then(data => {
        validateResponse(data);

        return Object.keys(data.servers).map(countryCode => {
            const { auth } = data;
            const location = data.servers[countryCode];
            const servers = (location && location.hosts) || [];

            return {
                countryCode,
                countryName: data.servers[countryCode].country_name,
                username: auth.username,
                password: auth.password,
                servers: servers,
                trial: false
            };
        });
    });
}

function validateResponse(data) {
    if (!data || typeof data !== 'object') {
        throw new apiErrors.ResponseError('Response type incorrect');
    }

    if (data.status === 'OK') {
        return true;
    }

    if (data.status !== 'error') {
        throw new apiErrors.ResponseError('Response status undefined');
    }

    switch (data.error) {
        case 'code.invalid': throw new apiErrors.CodeInvalidError(data.error_msg);
        case 'code.expired': throw new apiErrors.CodeExpiredError(data.error_msg);
        case 'code.limit': throw new apiErrors.CodeLimitError(data.error_msg);
        case 'vpn.order.error.email-alredy-registered': throw new apiErrors.EmailError(data.error_msg);
        case 'vpn.order.error.invalid-email-not-allowed': throw new apiErrors.EmailError(data.error_msg);
        case 'vpn.order.error.invalid-email': throw new apiErrors.EmailError(data.error_msg);
        default : throw new apiErrors.ResponseError(`Unknown response error - ${data.error}. ${data.error_msg}`);
    }
}

function getApiPath() {
    const lang = localeService.getLocale();
    return `${BACKEND_BASE_URL}/${lang}/vpn/api`;
}

export function getPromoDeal() {
    return axios.get(`${getApiPath()}/promo_deal`)
      .then(res => {
          const data = res.data;
          return {
              src: data.src,
          }
      })
}

export default {
    registerAccount,
    getAccountInfo,
    getTrialProxies,
    getPremiumProxies
};