

export function handleProxyRequest() {

    const proxy = window.browser["whoer_proxy"];
    if(proxy)
        return proxy.servers.map((val,index)=>
        {
            return {type: "http", host: val.host, port: val.port};
        });
    else
        return {type: "direct"};
  }

export function setProxy(proxy) {
    if (window.browser) {
        if(window.browser.proxy.onRequest){
                window.browser["whoer_proxy"] = proxy;
                return new Promise((resolve, reject) => {
                    if(!window.browser.proxy.onRequest.hasListener(handleProxyRequest))
                        window.browser.proxy.onRequest.addListener(handleProxyRequest, {urls: ["<all_urls>"]});
                    else
                        {
                            window.browser.proxy.onRequest.removeListener(handleProxyRequest);
                            window.browser.proxy.onRequest.addListener(handleProxyRequest, {urls: ["<all_urls>"]});
                        }
                    resolve()
                })

        }
            else{
                return window.browser.proxy.register('MozillaPac.js').then(() => {
                    return window.browser.runtime.sendMessage(
                        {type: 'setPacScriptProxy', payload: getConnectionString(proxy)},
                        {toProxyScript: true}
                    );
                });

        }
    }

    return new Promise((resolve, reject) => {
        try {
            window.chrome.proxy.settings.set({
                value: {
                    mode: "pac_script",
                    pacScript: {
                        data: getPacScript(proxy),
                        mandatory: true
                    }
                }
            }, resolve);
        }
        catch (err) {
            reject(err);
        }
    });
}

export function removeProxy() {
    if (window.browser) {
        if(window.browser.proxy.onRequest)
            return new Promise((resolve, reject) => {
                window.browser.proxy.onRequest.removeListener(handleProxyRequest);
                resolve()
            })
        else
            return window.browser.proxy.unregister();
    }

    return new Promise((resolve, reject) => {
        try {
            window.chrome.proxy.settings.clear({}, resolve)
        }
        catch (err) {
            reject(err)
        }
    });
}

export default {
    setProxy,
    removeProxy,
    handleProxyRequest
}



function getConnectionString(proxy) {
    const firstRandom = proxy.servers[Math.floor(Math.random()*proxy.servers.length)];
    const servers = [];
    servers[0] = firstRandom;
    [proxy.servers].forEach(element => {
        if(element.host !== firstRandom.host)
            servers[servers.length] = element
    });
    return 'PROXY ' + servers.map(srv => srv.host + ':' + srv.port).join('; ');
}

function getPacScript(proxy) {
    return (
        `function FindProxyForURL(url, host) {
            switch (host) {
                case 'secwhapi.net':
                case 'test.whoer.net':
                    return 'DIRECT';
                default:
                    return '${getConnectionString(proxy)}';
            }
        }`
    );
}
