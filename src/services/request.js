
const localBrowser = window.browser || window.chrome;

export const onBeforeRequest = localBrowser.webRequest.onBeforeRequest;
export const onBeforeSendHeaders = localBrowser.webRequest.onBeforeSendHeaders;
export const onAuthRequired = localBrowser.webRequest.onAuthRequired;
export const onErrorOccurred = localBrowser.webRequest.onErrorOccurred;
export const onCompleted = localBrowser.webRequest.onCompleted;

export default {
    onBeforeRequest,
    onBeforeSendHeaders,
    onAuthRequired,
    onErrorOccurred,
    onCompleted
}