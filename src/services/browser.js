
import { ConnectError } from "../errors/base";
import axios from "axios/index";

const CONNECT_CHECK_URL = process.env.WHOER_CONNECT_CHECK_URL;
const CONNECT_CHECK_TIMEOUT = +process.env.WHOER_CONNECT_CHECK_TIMEOUT;

const browser = window.browser || window.chrome;

const stateFields = [
    'trial',
    'premium',
    'account',
    'proxies',
    'proxy'
];

export function readState() {
    return new Promise(function (resolve, reject) {
        try {
            browser.storage.local.get(stateFields, data => {
                resolve(data);
            });
        }
        catch (err) {
            reject(err);
        }
    });
}

export function saveState(state) {
    return new Promise(function (resolve, reject) {
        try {
            const data = {};
            stateFields.forEach(key => data[key] = state[key]);
            browser.storage.local.set(data, () => {
                resolve();
            });
        }
        catch (err) {
            reject(err);
        }
    }).catch(err => {
        console.error('State save failed\n', err);
    });
}

export function openTab(url) {
    browser.tabs.create({url});
}

export function setProxy(server) {
    return window.browser ? setFirefoxProxy(server) : setChromeProxy(server);
}

export function clearProxy() {
    return window.browser ? clearFirefoxProxy() : clearChromeProxy();
}

export function checkConnect() {
    console.log('CONNECT_CHECK_URL', CONNECT_CHECK_URL);
    return axios.get(CONNECT_CHECK_URL, {timeout: CONNECT_CHECK_TIMEOUT}).catch(err => {
        throw new ConnectError(err.message);
    });
}

function setFirefoxProxy(server) {
    const data = {
        proxy: "PROXY " + server.host + ":" + server.port,
        username: server.username,
        password: server.password
    };

    const opts = {
        toProxyScript: true
    };

    return browser.proxy.register('MozillaPac.js').then(() => {
        return browser.runtime.sendMessage(data, opts)
    });
}

function setChromeProxy(server) {
    const data = {
        username: server.username,
        password: server.password
    };

    const opts = {
        value: {
            mode: "pac_script",
            pacScript: {
                data: `function FindProxyForURL(url, host) {return 'PROXY ${server.host}:${server.port}';}`
            }
        },
        scope: 'regular'
    };

    return Promise.resolve().then(() => {
        return new Promise((resolve, reject) => {
            try {browser.runtime.sendMessage(data, resolve)}
            catch (err) {reject(err);}
        })
    }).then(() => {
        return new Promise((resolve, reject) => {
            try {browser.proxy.settings.set(opts, resolve)}
            catch (err) {reject(err)}
        });
    })
}

function clearFirefoxProxy() {
    return browser.proxy.unregister();
}

function clearChromeProxy() {
    return new Promise((resolve, reject) => {
        try {browser.proxy.settings.clear({}, resolve)}
        catch (err) {reject(err)}
    });
}