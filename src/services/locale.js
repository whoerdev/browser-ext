
import axios from "axios";
import datex from 'data-expression';

let currentLocale;
let currentMessages;

const browser = window.browser || window.chrome;
const LOCALES = JSON.parse(process.env.WHOER_LOCALES);
const DEFAULT_LOCALE = process.env.WHOER_DEFAULT_LOCALE;


export function setLocale(code) {
    let lang;

    return Promise.resolve().then(() => {
        lang = code || getDefaultLocale();
        return getResource(lang);
    }).catch(err => {
        console.error('Locale set failed', err);
        lang = getDefaultLocale();
        return getResource(lang);
    }).then(messages => {
        currentLocale = lang;
        currentMessages = messages;
        return lang;
    });
}

export function getLocale() {
    const details = findLocaleDetails(currentLocale);
    return (details && details.code) || getDefaultLocale();
}

export function getMessage(name, substitutions) {
    const item = currentMessages && currentMessages[name];
    let msg = (item && item.message) || '';

    if (typeof substitutions === 'string' || typeof substitutions === 'number') {
        substitutions = [substitutions];
    }

    if (Array.isArray(substitutions)) {
        substitutions.forEach((val, index) => {
            const re = new RegExp('\\$' + (++index));
            msg = msg.replace(re, val);
        });
    }

    return msg;
}

function getPluralMessage(name, substitutions) {
    try {
        if (!Array.isArray(substitutions)) {
            substitutions = [substitutions];
        }
        const count = +substitutions[0] || 0;
        const postfixMap = JSON.parse(getMessage('pluralPostfixMap'));
        const postfix = postfixMap.reduce(makeSelectReducer(count), '');
        return getMessage(name + postfix, substitutions);
    }
    catch(err) {
        console.error('Plural message get failed\n', err);
        return '';
    }
    
    function makeSelectReducer(count) {
        const params = {
            n: +count,
            l1: +String(count).slice(-1),
            l2: +String(count).slice(-2)
        };

        return (res, item) => {
            const [key, exp] = item;

            if (!res && key && (!exp || datex(exp, params))) {
                return key;
            }

            return res;
        }
    }
}

export function getDetailLocales() {
    return LOCALES;
}

export default {
    setLocale,
    getLocale,
    getDetailLocales,
    getMessage,
    getPluralMessage,
    getLinkWithPrefix
}

function findLocaleDetails(code) {
    return LOCALES.find(locale => code && locale.code === code);
}

function getDefaultLocale() {
    const browserLocale = findLocaleDetails(browser.i18n.getUILanguage());
    const defaultLocale = findLocaleDetails(DEFAULT_LOCALE) || {code: 'en', name: 'english'};
    return (browserLocale && browserLocale.code) || defaultLocale.code;
}

function getResource(lang) {
    const path = '_locales/' + lang + '/messages.json';

    return axios.get(browser.runtime.getURL(path), {responseType: 'json'}).then(res => {
        if (!res || !res.data) {
            throw new Error(`Localization resource "${lang}" load error`);
        }
        return res.data;
    });
}


function getLinkWithPrefix(link) {
    const prefix = getLocale();
    const baseDomain = 'https://whoer.net/';
    return prefix === 'en' ? `${baseDomain}${link}` : `${baseDomain}${prefix}/${link}`;
    // if (prefix === 'en') {
    //     return link;
    // } else {
    //     return link.replace(baseDomain, `${baseDomain}/${prefix}`);
    // }
}