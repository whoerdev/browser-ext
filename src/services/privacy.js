

export function setWebRTCIPHandlingPolicy(value) {
    if (window.browser) {
        return window.browser.privacy.network.webRTCIPHandlingPolicy.set({value});
    }
    return new Promise((resolve, reject) => {
        try {
            window.chrome.privacy.network.webRTCIPHandlingPolicy.set({value}, function(data) {
                const err = window.chrome.runtime.lastError;
                err ? reject(err) : resolve(data);
            });
        }
        catch (err) {
            reject(err);
        }
    });
}

export default {
    setWebRTCIPHandlingPolicy
}