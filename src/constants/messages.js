
export const INIT_APP = 'init_app';
export const INIT_SUCCESS = 'init_success';
export const INIT_FAILURE = 'init_failure';
export const SET_PROXY = 'set_proxy';
export const REMOVE_PROXY = 'remove_proxy';
export const ENABLE_PROXY = 'enable_proxy';
export const DISABLE_PROXY = 'disable_proxy';
export const PROXY_CHANGED = 'proxy_changed';
export const SET_OPTIONS = 'set_options';
export const OPTIONS_CHANGED = 'options_changed';

