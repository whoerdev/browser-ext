
// import {CodeInvalidError, CodeExpiredError, EmailError} from '../../errors/api';
// import { AccessError } from '../../errors/base';

import {
  RESET_ERROR,
  REGISTRATION_TRIAL,
  CHANGE_EMAIL,
  REGISTRATION_TRIAL_FAILURE,
  REGISTRATION_TRIAL_LIMIT,
  TOGGLE_PRELOADER
} from '../constants/actions';
// import {REGISTRATION} from "../constants/pages";


//const defaultValue = {
//    codeInput: '',
//    valid: false,
//    error: null
//};

export default function (state = {}, action) {
  const { payload } = action;

  switch (action.type) {

    case CHANGE_EMAIL: {
      return {
        ...state,
        email: payload
      }
    }

    case REGISTRATION_TRIAL: {
      return {
        ...state
      }
    }

    case REGISTRATION_TRIAL_FAILURE: {
      return {
        ...state,
        error: payload
      }
    }

    case  REGISTRATION_TRIAL_LIMIT:{
      return {
        ...state,
        error: payload
      }
    }

    case TOGGLE_PRELOADER: {
      return {
        ...state,
        preloader: payload
      }
    }

    case RESET_ERROR: {
      return {
        ...state,
        error: null
      }
    }


    default: return state;
  }
}