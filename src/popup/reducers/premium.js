
import {
    INIT_APP_SUCCESS, SET_PREMIUM
}
    from "../constants/actions";

import {
    //ACTIVATE_PREMIUM_SUCCESS,
    CHECK_PREMIUM_SUCCESS
}
from "../constants/actions";


export default function (state = null, action) {
    const { payload } = action;

    switch (action.type) {

        case INIT_APP_SUCCESS:
            return payload.premium || null;

        case SET_PREMIUM:
            return payload || null;

        case CHECK_PREMIUM_SUCCESS:
            return {
                ...payload,
                enabled: state.enabled
            };

        default: return state;
    }
}
