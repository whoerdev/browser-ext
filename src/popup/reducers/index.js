
import { combineReducers } from 'redux';

import navigation from './navigation';
import trial from './trial';
import premium from './premium';
import options from './options';
import activation from './activation';
import locations from './locations.js';
import proxy from './proxy';
import delay from './delay';
import error from './error';
import login from "./login";
import competitor from "./competitor";
import registration from "./registration";
import firstStartBrowser from "./firstStartBrowser";


export default combineReducers({
    navigation,
    activation,
    locations,
    options,
    trial,
    premium,
    proxy,
    delay,
    error,
    login,
    competitor,
    registration,
    firstStartBrowser,
});
