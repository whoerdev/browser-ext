
import {
    INIT_APP_SUCCESS,
    SET_OPTIONS
}
from '../constants/actions';


export default function (state = {}, action) {
    const { payload } = action;

    switch (action.type) {

        case INIT_APP_SUCCESS:
            return Object.assign({}, payload.options);

        case SET_OPTIONS:
            return Object.assign({}, payload);

        default: return state;
    }
}
