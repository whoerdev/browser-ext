import {DISABLE_COMPETITORS, FOUND_COMPETITORS} from "../constants/actions";

export default function (state = [], action) {
  const {payload} = action;

  switch (action.type) {
    case FOUND_COMPETITORS:
      return payload || null
    case DISABLE_COMPETITORS:
      return null

    default: return state
  }
}