
import { CodeInvalidError, CodeExpiredError } from '../../errors/api';
import { AccessError } from '../../errors/base';

import {
    INIT_APP,
    INIT_APP_FAILURE,
    ACTIVATE_PREMIUM,
    ACTIVATE_PREMIUM_FAILURE,
    SET_PROXY,
    NAVIGATION_OPEN,
    NAVIGATION_BACK,
    RESET_ERROR,
    FETCH_PROXIES_FAILURE,
    FETCH_PROXIES,
    PROXY_ERROR
}
from "../constants/actions";






export default function (state = null, action) {
    const { payload } = action;

    console.log(action.type, payload);

    switch (action.type) {

        case SET_PROXY:
            // if (payload && payload.enabled) {
            //     if (payload.trial)
            //         browser.browserAction.setIcon({ path: "on.png" });
            //     else
            //         browser.browserAction.setIcon({ path: "on.png" });
            // } else {
            //     browser.browserAction.setIcon({ path: "off.png" });
            // }
            return null;
        case INIT_APP:
        case FETCH_PROXIES:
        case NAVIGATION_OPEN:
        case NAVIGATION_BACK:
        case ACTIVATE_PREMIUM:
        case RESET_ERROR:
            return null;

        case INIT_APP_FAILURE:
        case FETCH_PROXIES_FAILURE:
            return {
                action: action.type,
                message: payload.message,
                params: []
            };

        case PROXY_ERROR:
            return {
                action: action.type,
                message: payload.message,
                params: [payload.proxy.countryCode]
            };

        case ACTIVATE_PREMIUM_FAILURE:
            if (payload instanceof CodeExpiredError) return state;
            if (payload instanceof CodeInvalidError) return state;
            if (payload instanceof AccessError) return state;

            return {
                action: action.type,
                name: payload.name,
                message: payload.message,
                params: []
            };

        default: return state;
    }
}
