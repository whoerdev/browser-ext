
import {INIT_APP_SUCCESS, UPDATE_DELAY} from '../constants/actions';


export default function (state = 0, action) {
    const { payload } = action;

    switch (action.type) {

        case INIT_APP_SUCCESS:
            return payload.delay;

        case UPDATE_DELAY:
            return 0; //payload

        default: return state;
    }
}
