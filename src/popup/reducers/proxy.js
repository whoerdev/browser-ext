
import {
    INIT_APP_SUCCESS,
    SET_PROXY,
    //ENABLE_PROXY,
    //DISABLE_PROXY
}
from "../constants/actions";


export default function (state = null, action) {
    const { payload } = action;

    switch (action.type) {

        case INIT_APP_SUCCESS:
            return payload.proxy || null;

        case SET_PROXY:
            return payload || null;

        // case SET_PROMO_URL:
        //     return {
        //         ...state,
        //         promo: payload
        //     }

        default: return state;
    }
}
