
import { CodeInvalidError, CodeExpiredError } from '../../errors/api';
import { AccessError } from '../../errors/base';

import {
    INIT_APP_SUCCESS,
    CHANGE_ACTIVATION_CODE,
    CHECK_PREMIUM_SUCCESS,
    CHECK_PREMIUM_FAILURE,

    ACTIVATE_PREMIUM,
    ACTIVATE_PREMIUM_SUCCESS,
    ACTIVATE_PREMIUM_FAILURE, DEACTIVATE_PREMIUM
}
    from '../constants/actions';


//const defaultValue = {
//    codeInput: '',
//    valid: false,
//    error: null
//};

export default function (state = {}, action) {
    const { payload } = action;

    switch (action.type) {

        case INIT_APP_SUCCESS:
            return {
                ...state,
                // codeInput: (payload.premium && payload.premium.code) || ''
            };

        case CHANGE_ACTIVATION_CODE:
            return {
                ...state,
                codeInput: payload,
                valid: false,
                error: null
            };

        case ACTIVATE_PREMIUM:
        case DEACTIVATE_PREMIUM:
            return {
                ...state,
                valid: false,
                error: null
            };

        case ACTIVATE_PREMIUM_SUCCESS:
        case CHECK_PREMIUM_SUCCESS:
            return {
                ...state,
                valid: true,
                error: null
            };

        case ACTIVATE_PREMIUM_FAILURE:
        case CHECK_PREMIUM_FAILURE: {
            let error = null;
            if (payload instanceof CodeInvalidError) error = payload;
            if (payload instanceof CodeExpiredError) error = payload;
            if (payload instanceof AccessError) error = payload;

            return {
                ...state,
                valid: false,
                error
            };
        }


        default: return state;
    }
}