
import { CodeInvalidError, CodeExpiredError } from '../../errors/api';
import { AccessError } from '../../errors/base';

import {
  CHECK_PREMIUM_FAILURE,

  ACTIVATE_PREMIUM_FAILURE, RESET_ERROR, TOGGLE_PRELOADER
}
  from '../constants/actions';


//const defaultValue = {
//    codeInput: '',
//    valid: false,
//    error: null
//};

export default function (state = {}, action) {
  const { payload } = action;

  switch (action.type) {

    case ACTIVATE_PREMIUM_FAILURE:
    case CHECK_PREMIUM_FAILURE: {
      let error = null;
      if (payload instanceof CodeInvalidError) error = payload;
      if (payload instanceof CodeExpiredError) error = payload;
      if (payload instanceof AccessError) error = payload;

      return {
        ...state,
        valid: false,
        error
      };
    }

    case RESET_ERROR: {
      return {
        ...state,
        error: null
      }
    }

    case TOGGLE_PRELOADER: {
      return {
        ...state,
        preloader: payload
      }
    }


    default: return state;
  }
}