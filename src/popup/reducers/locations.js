
import {
    CHANGE_FAVORITES,
    CHANGE_PROXIES_FILTER, FETCH_PROXIES, FETCH_PROXIES_FAILURE, FETCH_PROXIES_SUCCESS, PENDING_IP, SET_IP_INFO
}
    from "../constants/actions";

//import { UPDATE_PROXIES, UPDATE_PROXIES_SUCCESS, UPDATE_PROXIES_FAILURE } from "../constants/actions";


const defaultState = {
    filter: '',
    proxies: [],
    error: null,
    favorites: []
};

export default function proxies(state = defaultState, action) {
    const { payload } = action;

    switch (action.type) {

        // case INIT_APP:
        //     return payload && payload.proxies || state;

        // case INIT_TRIAL_SUCCESS:
        // case INIT_PREMIUM_SUCCESS:
        // case ACTIVATE_PREMIUM_SUCCESS:
        //     return payload.proxies;

        case CHANGE_PROXIES_FILTER:
            return {
                ...state,
                filter: payload
            };

        case FETCH_PROXIES:
            return {
                ...state,
                error: null
            };

        case FETCH_PROXIES_SUCCESS:
            return  {
                ...state,
                proxies: payload,
                error: null
            };

        case FETCH_PROXIES_FAILURE:
            return {
                ...state,
                error: payload
            };

        case CHANGE_FAVORITES:
            return {
                ...state,
                favorites: payload
            }

        case PENDING_IP:
            return {
                ...state,
                ipInfo: null
            }

        case SET_IP_INFO:
            return {
                ...state,
                ipInfo: {...payload},
            }

        default: return state;
    }
}
