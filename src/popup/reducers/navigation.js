
import { NAVIGATION_OPEN, NAVIGATION_BACK } from '../constants/actions';
//import { PROXY_PAGE } from '../constants/pages';


const defaultState = {
    page: '',
    history: []
};

export default function (state = defaultState, action) {
    const { payload } = action;

    switch (action.type) {

        case NAVIGATION_OPEN: {
            if (state.page === payload) {
                return state;
            }

            return {
                page: payload,
                history: [...state.history, payload]
            };
        }

        case NAVIGATION_BACK: {
            const history = [...state.history];
            if (history.length < 2) return state;
            history.pop();

            return {
                page: history[history.length - 1],
                history: history
            };
        }

        default:
            return state;
    }
}
