
import {INIT_APP_SUCCESS, RESET_TRIAL, SET_TRIAL} from '../constants/actions';


export default function (state = null, action) {
    const { payload } = action;


    switch (action.type) {

        case INIT_APP_SUCCESS:
            return payload.trial || null;

        case SET_TRIAL:
            return payload || null;

        case RESET_TRIAL:
            return null;


        default: return state;
    }
}
