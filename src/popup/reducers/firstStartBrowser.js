import {FIRST_START_BROWSER} from '../constants/actions';

export default function (state={firstStartBrowser: false}, action) {
  const { payload } = action;

  switch (action.type) {

    case FIRST_START_BROWSER: {
      return {
        ...state,
        firstStartBrowser: payload,
      };
    }

    default: return state;
  }
}