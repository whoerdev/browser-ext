export const BACKEND_BASE_URL = 'https://whoer.net';
export const BACKEND_URL = BACKEND_BASE_URL + '/en/vpn/api';
export const HELP_CENTER_URL = 'https://whoer.net';
export const GET_PREMIUM_URL = 'https://whoer.net';
export const CONNECT_CHECK_URL = 'https://google.com';
export const CONNECT_CHECK_TIMEOUT = 4000;
export const REQUEST_TIMEOUT = 5000;