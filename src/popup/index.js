import React /*, {Component, Children}*/ from 'react';
//import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App';
import { applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore } from 'redux';
import { Provider/*, connect*/ } from 'react-redux';
import reducer from './reducers/index.js';


console.log('ENV', process.env);

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));


setTimeout(() => {
    ReactDOM.render(
        <Provider store={store}>
            <App />
        </Provider>,
        document.getElementById('root')
    );
}, 100);
