import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as commonActions from '../actions/common';
import * as navActions from '../actions/navigation';
import SubscriptionLowTime from "../components/SubscriptionLowTime";
import * as popupHelpers from "../helpers";

const mapStateToProps = state => ({
  account: popupHelpers.collectAccount(state),
});

const mapDispatchToProps = dispatch => ({
  commonActions: bindActionCreators(commonActions, dispatch),
  navActions: bindActionCreators(navActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(SubscriptionLowTime);
