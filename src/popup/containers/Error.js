
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as commonActions from '../actions/common';
import * as navActions from '../actions/navigation';
import Error from '../components/Error';
import localeService from '../../services/locale';
import WarningImage from '../images/error.svg';
import ProxiesErrorImage from '../images/proxies-error.svg';

import {
    FETCH_PROXIES_FAILURE,
    INIT_APP_FAILURE,
    ACTIVATE_PREMIUM_FAILURE,
    PROXY_ERROR,
    REGISTRATION_TRIAL_LIMIT
} from '../constants/actions';


const mapStateToProps = state => ({
    error: state.error
});

const mapDispatchToProps = dispatch => ({
    commonActions: bindActionCreators(commonActions, dispatch),
    navActions: bindActionCreators(navActions, dispatch)
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const { error } = stateProps;
    const { commonActions, navActions } = dispatchProps;
    let image = WarningImage;
    let title = localeService.getMessage('connectionErrorTitle');
    let message = localeService.getMessage('connectionErrorText');
    let onBack, onRetry;

    switch (error.action) {
        case INIT_APP_FAILURE:
            onRetry = commonActions.initApp;
            break;

        case FETCH_PROXIES_FAILURE:
            title = localeService.getMessage('fetchProxiesErrorTitle');
            message = localeService.getMessage('fetchProxiesErrorText');
            image = ProxiesErrorImage;
            onRetry = () => {
                commonActions.checkPremium();
                commonActions.fetchProxies();
            };
            break;

        case PROXY_ERROR:
            onBack = () => navActions.openProxies();
            onRetry = () => commonActions.selectProxy.apply(null, error.params);
            break;

        case ACTIVATE_PREMIUM_FAILURE:
            if (error.name === 'CodeLimitError') {
                message = error.message || message;
            }
            onBack = commonActions.resetError;
            onRetry = commonActions.activatePremium;
            break;
        case REGISTRATION_TRIAL_LIMIT:
            if (error.name === 'CodeLimitError') {
                message = error.message || message;
            }
            onBack = commonActions.resetError;
            onRetry = commonActions.registerTrial;
            break;
        default:
            break;
    }

    return Object.assign({}, ownProps, stateProps, {
        image,
        title,
        message,
        onBack,
        onRetry
    });
};

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Error);
