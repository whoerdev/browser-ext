import { connect } from 'react-redux';
import * as commonActions from '../actions/common';
import * as navActions from '../actions/navigation';
import Competitor from "../components/Competitor";
import {bindActionCreators} from "redux";

const mapStateToProps = state => ({
  locale: state.options && state.options.locale,
  competitors: state.competitor,
});

const mapDispatchToProps = dispatch => ({
  commonActions: bindActionCreators(commonActions, dispatch),
  navActions: bindActionCreators(navActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Competitor);
