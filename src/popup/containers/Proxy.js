
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as commonActions from '../actions/common';
import * as navActions from '../actions/navigation';
import Proxy from '../components/Proxy';
import * as popupHelpers from '../helpers';

const mapStateToProps = state => ({
    account: popupHelpers.collectAccount(state),
    proxy: state.proxy,
    delay: state.delay,
    proxyEnabled: state.proxy && state.proxy.enabled,
    ipInfo: state.locations.ipInfo,
    firstStartBrowser: state.firstStartBrowser
});

const mapDispatchToProps = dispatch => ({
    commonActions: bindActionCreators(commonActions, dispatch),
    navActions: bindActionCreators(navActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Proxy);
