import { connect } from 'react-redux';
import * as commonActions from '../actions/common';
import * as navActions from '../actions/navigation';
import Registration from "../components/Registration";
import {bindActionCreators} from "redux";

const mapStateToProps = state => ({
  locale: state.options && state.options.locale,
  registration: state.registration
});

const mapDispatchToProps = dispatch => ({
  commonActions: bindActionCreators(commonActions, dispatch),
  navActions: bindActionCreators(navActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Registration);
