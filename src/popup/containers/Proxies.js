
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as commonActions from '../actions/common';
import * as navActions from '../actions/navigation';
import Proxies from '../components/Proxies';
import * as popupHelpers from "../helpers";


const mapStateToProps = state => {
    const locations = state.locations;
    const proxySelector = createProxySelector(locations.filter);
    const proxyFormatter = createProxyFormatter(state.proxy);

    return {
        locale: state.options && state.options.locale,
        proxies:  locations.proxies.filter(proxySelector).map(proxyFormatter).sort(proxySorter),
        filter: locations.filter,
        error: locations.error,
        account: popupHelpers.collectAccount(state),
        favorites: locations.favorites
    }
};

const mapDispatchToProps = dispatch => ({
    commonActions: bindActionCreators(commonActions, dispatch),
    navActions: bindActionCreators(navActions, dispatch)
});

function createProxySelector(filter) {
    const value = filter.trim().toLowerCase();
    return proxy => proxy.countryName.toLowerCase().indexOf(value) !== -1;
}

function createProxyFormatter(currentProxy) {
    return proxy => Object.assign({}, proxy, {
       selected: currentProxy && currentProxy.enabled && currentProxy.countryCode === proxy.countryCode
    });
}

function proxySorter(a, b) {
    if (a.trial && !b.trial) return -1;
    if (!a.trial && b.trial) return 1;
    return 0;
}

export default connect(mapStateToProps, mapDispatchToProps)(Proxies);
