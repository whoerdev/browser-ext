import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as commonActions from '../actions/common';
import * as navActions from '../actions/navigation';
import RegistrationNotification from "../components/RegistrationNotification";

const mapStateToProps = state => ({
  locale: state.options && state.options.locale
});

const mapDispatchToProps = dispatch => ({
  commonActions: bindActionCreators(commonActions, dispatch),
  navActions: bindActionCreators(navActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationNotification);
