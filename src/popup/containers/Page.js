
import { connect } from 'react-redux';
import * as commonActions from '../actions/common';
import * as navActions from '../actions/navigation';
import Page from '../components/Page';
// import {bindActionCreators} from "redux";


const mapStateToProps = state => ({
    proxyEnabled: state.proxy && state.proxy.enabled,
    proxyAvailable: !!state.proxy,
    testState: state,
});

const mapDispatchToProps = dispatch => {
    return {
        onProxyToggle: () => dispatch(commonActions.toggleProxy()),
        onOptionsClick: () => dispatch(navActions.openOptions()),
        backPage: () => dispatch(navActions.backPage())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Page);
