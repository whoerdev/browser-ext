
import { connect } from 'react-redux';
import Localize from '../components/Localize';


const mapStateToProps = state => ({
    locale: state.options && state.options.locale
});


export default connect(mapStateToProps)(Localize);
