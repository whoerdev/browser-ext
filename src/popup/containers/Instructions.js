import { connect } from 'react-redux';
import * as commonActions from '../actions/common';
import * as navActions from '../actions/navigation';
import Instructions from "../components/Instructions";
import {bindActionCreators} from "redux";
import localService from "../../services/locale";

const mapStateToProps = state => {
    return {
        locales: localService.getDetailLocales(),
        options: Object.assign({locale: localService.getLocale()}, state.options),
        instructions:  state.instructions
    }
};

const mapDispatchToProps = dispatch => ({
    commonActions: bindActionCreators(commonActions, dispatch),
    navActions: bindActionCreators(navActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Instructions);