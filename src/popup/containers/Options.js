
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as commonActions from '../actions/common';
import * as navActions from '../actions/navigation';
import Options from '../components/Options';
import localService from '../../services/locale';
import * as popupHelpers from "../helpers";

//const LOCALES = process.env.WHOER_LOCALES;


const mapStateToProps = state => {
    return {
        locales: localService.getDetailLocales(),
        options: Object.assign({locale: localService.getLocale()}, state.options),
        account: popupHelpers.collectAccount(state),
    }
};

const mapDispatchToProps = dispatch => ({
    commonActions: bindActionCreators(commonActions, dispatch),
    navActions: bindActionCreators(navActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Options);
