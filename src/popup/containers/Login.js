import { connect } from 'react-redux';
import * as commonActions from '../actions/common';
import * as navActions from '../actions/navigation';
import Login from "../components/Login";
import {bindActionCreators} from "redux";

const mapStateToProps = state => ({
  locale: state.options && state.options.locale,
  login: state.login,
  test: state
});

const mapDispatchToProps = dispatch => ({
  commonActions: bindActionCreators(commonActions, dispatch),
  navActions: bindActionCreators(navActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
