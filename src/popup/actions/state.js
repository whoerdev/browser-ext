
import storageService from '../../services/storage';
import { INIT_STATE } from "../constants/actions";


const STORAGE_FIELDS = [
    'premium',
    'account'
];


export const initState = (data) => (dispatch, getState) => {
    return storageService.getItems(STORAGE_FIELDS).then(state => {
        Object.assign(state, data);
        dispatch({type: INIT_STATE, payload: state});
    });
};

export const saveState = () => (dispatch, getState) => {
    return Promise.resolve();
};