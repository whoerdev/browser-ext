
import * as PAGES  from "../constants/pages";
import {NAVIGATION_OPEN, NAVIGATION_BACK, FIRST_START_BROWSER} from "../constants/actions";
import storageService from "../../services/storage";
// import {SUBSCRIPTION_EXPIRED_PAGE} from "../constants/pages";


const PAGES_LIST = Object.keys(PAGES).reduce((list, key) => {
    list.push(PAGES[key]);
    return list;
}, []);


export const openPage = (name) => (dispatch, getState) => {
    if (PAGES_LIST.includes(name)) {
        dispatch({type: NAVIGATION_OPEN, payload: name});
    }
};

export const openProxy = () => (dispatch, getState) => {
    dispatch(openPage(PAGES.PROXY_PAGE));
};

export const openProxies = () => (dispatch, getState) => {
    dispatch(openPage(PAGES.PROXIES_PAGE));
};

export const openActivation = () => (dispatch, getState) => {
    dispatch(openPage(PAGES.ACTIVATION_PAGE));
};

export const openOptions = () => (dispatch, getState) => {
    dispatch(openPage(PAGES.OPTIONS_PAGE));
};

export const backPage = () => (dispatch, getState) => {
    dispatch({type: NAVIGATION_BACK});
};

export const openStart = () => (dispatch, getState) => {
    dispatch(openPage(PAGES.START_PAGE));
};
export const openRegistration = () => (dispatch, getState) => {
    dispatch(openPage(PAGES.REGISTRATION));
};
export const openLogin = () => (dispatch, getState) => {
    dispatch(openPage(PAGES.LOGIN));
};
export const openRegistrationNotification = () => (dispatch, getState) => {
    dispatch(openPage(PAGES.REGISTRATION_NOTIFICATION));
};
export const openCompetitors = () => (dispatch, getState) => {
    dispatch(openPage(PAGES.COMPETITORS_PAGE));
};
export const openSubscription = () => (dispatch, getState) => {
    dispatch(openPage(PAGES.SUBSCRIPTION_PAGE));
    dispatch({type: FIRST_START_BROWSER, payload: false});
    storageService.setItem('firstStartBrowser', false);
};
export const openSubscriptionLowTime = () => (dispatch, getState) => {
    dispatch(openPage(PAGES.SUBSCRIPTION_LOW_TIME_PAGE));
    dispatch({type: FIRST_START_BROWSER, payload: false});
    storageService.setItem('firstStartBrowser', false);
};
export const openSubscriptionExpired = () => (dispatch, getState) => {
    dispatch(openPage(PAGES.SUBSCRIPTION_EXPIRED_PAGE));
};
export const openInstructions = () => (dispatch, getState) => {
    dispatch(openPage(PAGES.INSTRUCTIONS));
};