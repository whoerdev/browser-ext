import { AccessError } from '../../errors/base';
import { CodeInvalidError, CodeExpiredError, EmailError } from '../../errors/api';
import * as apiService from '../../services/api';
import messageService from '../../services/message';
import storageService from '../../services/storage';
import localeService, {getMessage} from '../../services/locale';
import Account from '../../models/account';
import Proxy from '../../models/proxy';
import * as navActions from './navigation';
import * as popupHelpers from '../helpers';
import detectService from '../../services/extensionDetection'

import {
    INIT_APP,
    INIT_APP_SUCCESS,
    INIT_APP_FAILURE,

    CHECK_PREMIUM,
    CHECK_PREMIUM_SUCCESS,
    CHECK_PREMIUM_FAILURE,

    ACTIVATE_PREMIUM,
    ACTIVATE_PREMIUM_SUCCESS,
    ACTIVATE_PREMIUM_FAILURE,
    DEACTIVATE_PREMIUM,

    FETCH_PROXIES,
    FETCH_PROXIES_SUCCESS,
    FETCH_PROXIES_FAILURE,

    CHANGE_ACTIVATION_CODE,
    CHANGE_PROXIES_FILTER,

    RESET_ERROR, //INIT_STATE,

    SET_TRIAL,
    SET_PROXY,
    SET_PREMIUM,
    SET_OPTIONS,
    UPDATE_DELAY,
    PROXY_ERROR,
    CHECK_KEY,
    RESET_TRIAL,
    FOUND_COMPETITORS,
    DISABLE_COMPETITORS,
    CHANGE_FAVORITES,
    CHANGE_EMAIL,
    REGISTRATION_TRIAL_FAILURE,
    REGISTRATION_TRIAL_LIMIT,
    TOGGLE_PRELOADER,
    SET_IP_INFO,
    PENDING_IP,
    FIRST_START_BROWSER

} from "../constants/actions";


import { collectAccount } from "../helpers";
import {
    openInstructions,
    openProxy,
    openRegistrationNotification,
    openSubscriptionExpired
} from "./navigation";
// import premium from "../reducers/premium";
// import trial from "../reducers/trial";
// import login from "../reducers/login";
// import {data} from "autoprefixer";

const REGISTRATION_DELAY = process.env.WHOER_REGISTRATION_DELAY;
const PREVENT_WEBRTC_LEAK_DEFAULT = process.env.WHOER_PREVENT_WEBRTC_LEAK_DEFAULT === 'true';
const DISABLE_FLASH_DEFAULT = process.env.WHOER_DISABLE_FLASH_DEFAULT === 'true';
const ADJUST_TIMEZONE_DEFAULT = process.env.WHOER_ADJUST_TIMEZONE_DEFAULT === 'true';

const defaultOptions = {
    preventWebRTCLeak: PREVENT_WEBRTC_LEAK_DEFAULT,
    disableFlash: DISABLE_FLASH_DEFAULT,
    adjustTimezone: ADJUST_TIMEZONE_DEFAULT
};


export const initApp = () => (dispatch, getState) => {

    dispatch({type: INIT_APP});

    Promise.resolve().then(() => {
        return storageService.getItems(['proxy', 'trial', 'premium', 'options', 'favorites','firstStartBrowser']);
    }).then(data => {
        const {proxy, trial, premium} = data;
        if(data.firstStartBrowser) {
            dispatch({type: FIRST_START_BROWSER, payload:  true});
            storageService.setItem('firstStartBrowser', true);
        }
        const browser=window.browser || window.chrome;
        data.proxy = Proxy.validate(proxy) ? proxy : null;
        if(data.proxy && data.proxy.enabled) {
            browser.browserAction.setIcon({path: "/on.png"});
        } else {
            browser.browserAction.setIcon({path: "/off.png"})
        }
        data.trial = Account.validate(trial) ? trial : null;
        data.premium = Account.validate(premium) ? premium : null;
        dispatch(saveFavorites(data.favorites || []));
        return presetOptions(data.options).then(options => ({...data, options}));
    }).then(data => {
        const acct = collectAccount(data);
        const delay = countRegistrationDelay((acct && acct.created) || Date.now());
        messageService.sendCustomMessage('checkStatus');
        dispatch({type: INIT_APP_SUCCESS, payload: {...data, delay}});
        // dispatch(getIp());
        dispatch(checkNextPage(acct));
    }).catch(err => {
        dispatch({type: INIT_APP_FAILURE, payload: err});
    });
};

export const resetTrial = () => (dispatch, getState) => {
    dispatch({ type: RESET_TRIAL });
};

export const checkNextPage = (account) => (dispatch, getState) => {

    switch (true) {

        case (account !== null && account.expires <= Date.now()):
            dispatch(openSubscriptionExpired());
            break;
        case (account !== null):
            dispatch(checkCompetitor());
            break;
        default:
            dispatch(navActions.openRegistration());
    }
}

export const checkCompetitor = () => (dispatch, getState) => {
    detectService.detectCompetitor()
      .then(data => {
          if (data.length > 0) {
              dispatch({type: FOUND_COMPETITORS, payload: data});
              dispatch(navActions.openCompetitors());
          } else {
              dispatch(navActions.openProxy());
          }
          dispatch(fetchProxies());
      })
}

export const registerTrial = () => (dispatch, getState) => {
    const { registration } = getState();
    dispatch(togglePreloader(true));
    Promise.resolve()
      .then(() => {
          if (!registration.email) throw new EmailError(getMessage('errorMessageEmpty'));
          return apiService.registerAccount(registration.email);
      })
      .then((data) => {
          dispatch(togglePreloader());
          dispatch(openRegistrationNotification());
      })
      .catch(err => {
          dispatch({type: REGISTRATION_TRIAL_FAILURE, payload: err});
          dispatch({type: REGISTRATION_TRIAL_LIMIT, payload: err});
          dispatch(togglePreloader());
      })
};

export const changeEmail = (email) => (dispatch, getState) => {
    dispatch({type: CHANGE_EMAIL, payload: email});
}
export const togglePreloader = (value = false) => (dispatch, getState) => {
    dispatch({type: TOGGLE_PRELOADER, payload: value});
}

export const resetProxy = () => (dispatch, getState) => {
    const { proxy, locations } = getState();
    const newProxy = proxy && locations.proxies.find(isEqual);
    const data = (newProxy && {...newProxy, enabled: proxy.enabled}) || null;
    dispatch(setProxy(data));

    function isEqual(item) {
        return item.countryCode === proxy.countryCode && item.servers.length;
    }
};

export const checkPremium = () => (dispatch, getState) => {
    const { premium } = getState();

    if (!premium || !premium.enabled) {
        return;
    }

    dispatch({type: CHECK_PREMIUM});

    apiService.getAccountInfo(premium.code).then(acct => {
        dispatch({type: CHECK_PREMIUM_SUCCESS, payload: acct});
    }).catch(err => {
        dispatch({type: CHECK_PREMIUM_FAILURE, payload: err});
    });
};

export const fetchProxies = () => (dispatch, getState) => {
    const acct = popupHelpers.collectAccount(getState());

    dispatch({type: FETCH_PROXIES});

    return Promise.resolve().then(() => {
        // return acct || dispatch(resetTrial());
        // return acct || dispatch(resetTrial());
        return acct;
    }).then(acct => {
        return getProxies(acct).catch(err => {
            if (acct.trial || !isCodeError(err)) return Promise.reject(err);
            dispatch(navActions.openActivation());
            dispatch(setPremium({...acct, enabled: false}));
            acct = popupHelpers.collectAccount(getState());
            if (!acct) return Promise.reject(err);
            console.log('from fetch with getProxies', getProxies(acct.code));
            return getProxies(acct.code);
        }).catch(err => {
            if (!isCodeError(err)) return Promise.reject(err);
            // if (!acct || acct.trial) return dispatch(resetTrial()).then(getProxies);
        });
    }).then(proxies => {
        dispatch({type: FETCH_PROXIES_SUCCESS, payload: proxies});
        dispatch(resetProxy());
    }).catch(err => {
        dispatch({type: FETCH_PROXIES_FAILURE, payload: err});
    });

    function getProxies(acct) {
        return acct.trial ? apiService.getTrialProxies(acct.code) : apiService.getPremiumProxies(acct.code);
    }

    function isCodeError(err) {
        return err instanceof CodeInvalidError || err instanceof CodeExpiredError;
    }
};

export const activatePremium = () => (dispatch, getState) => {
    const { activation } = getState();
    const code = activation.codeInput;

    dispatch({type: ACTIVATE_PREMIUM});

    return Promise.resolve().then(() => {
        if (!code) throw new CodeInvalidError();
        return apiService.getAccountInfo(code);
    }).then(account => {
        if (account.trial) throw new AccessError('Is not premium code');
        dispatch({type: ACTIVATE_PREMIUM_SUCCESS});
        dispatch(setPremium({...account, enabled: true}));
        dispatch(fetchProxies());
        // dispatch(navActions.backPage());
        dispatch(navActions.openProxy());
    }).catch(err => {
        dispatch({type: ACTIVATE_PREMIUM_FAILURE, payload: err});
    });
};

export const checkPremiumInput = () => (dispatch, getState) => {
    const { activation } = getState();
    const code = activation.codeInput;

    dispatch({ type: ACTIVATE_PREMIUM });

    return Promise.resolve().then(() => {
        if (!code) throw new CodeInvalidError();
        return apiService.getAccountInfo(code);
    }).then(account => {
        if (account.trial) throw new AccessError('Is not premium code');
        dispatch({ type: ACTIVATE_PREMIUM_SUCCESS });
        dispatch(setPremium({ ...account, enabled: true }));
        dispatch(fetchProxies());
    }).catch(err => {
        dispatch({ type: ACTIVATE_PREMIUM_FAILURE, payload: err });
    });
};

export const deactivatePremium = () => (dispatch, getState) => {
    const { premium } = getState();
    if (!premium) return;
    dispatch({type: DEACTIVATE_PREMIUM});
    dispatch(setPremium({...premium, enabled: false}));
    // dispatch(fetchProxies());
};

export const enableProxy = () => (dispatch, getState) => {
    const { proxy } = getState();
    const data = proxy && {...proxy, enabled: true};

    if (!data) return;
    dispatch(setProxy(data));
};

export const disableProxy = () => (dispatch, getState) => {
    const { proxy } = getState();
    const data = proxy && {...proxy, enabled: false};

    if (!data) return;
    dispatch(setProxy(data));
};

export const toggleProxy = () => (dispatch, getState) => {
    const { proxy } = getState();

    if (!proxy) return;
    dispatch(!proxy.enabled ? enableProxy() : disableProxy());
};

export const selectProxy = countryCode => (dispatch, getState) => {
    const { locations } = getState();
    const proxy = locations.proxies.find(proxy => proxy.countryCode === countryCode);
    const data = proxy && {...proxy, enabled: true};

    if (!data) {
        return;
    }

    if (!data.servers.length) {
        // dispatch(navActions.openActivation()); //if select country without premium account
        dispatch(navActions.openSubscription()); //if select country without premium account
        return;
    }

    dispatch(setProxy(data));
    dispatch(navActions.openProxy());
};

export const changeActivationCode = (value) => (dispatch, getState) => {
    dispatch({type: CHANGE_ACTIVATION_CODE, payload: value.trim()});
};

export const changeLocationsFilter = (value) => (dispatch, getState) => {
    dispatch({type: CHANGE_PROXIES_FILTER, payload: value});
};

export const resetError = () => (dispatch, getState) => {
    dispatch({type: RESET_ERROR});
};

export const setProxy = (data) => (dispatch, getState) => {
    dispatch({type: SET_PROXY, payload: data || null});
    // console.log('setProxy', data);
    return storageService.setItem('proxy', null).then(() => {
        return data && storageService.setItem('proxy', data).then(()=>{dispatch(getIp());});
    });
};

export const rejectProxy = (error) => (dispatch, getState) => {
    const { proxy } = getState();

    if (proxy && error.proxy && proxy.countryCode === error.proxy.countryCode) {
        dispatch({type: PROXY_ERROR, payload: error});
    }
};

export const setTrial = (data) => (dispatch, getState) => {
    dispatch({type: SET_TRIAL, payload: data || null});
    return storageService.setItem('trial', data || null);
};

export const setPremium = (data) => (dispatch, getState) => {
    dispatch({type: SET_PREMIUM, payload: data || null});
    return storageService.setItem('premium', data || null);
};

export const setOption = (name, value) => (dispatch, getState) => {
    const { options } = getState();
    const opts = {...options, [name]: value};
    return storageService.setItem('options', opts);
};

export const updateState = (data) => (dispatch, getState) => {
    Object.keys(data).forEach(key => {
        const item = data[key];

        switch (key) {
            case 'options': {
                const oldOptions = getState().options;
                const oldLocale = oldOptions && oldOptions.locale;
                presetOptions(item).then(options => {
                    dispatch({type: SET_OPTIONS, payload: options});
                    if (options.locale !== oldLocale) dispatch(fetchProxies());
                }).catch(err => {
                    console.error('Update options failed\n', err);
                });
                break;
            }
            default: {
                break;
            }
        }
    });
};

export const updateDelay = (delay) => (dispatch, getState) => {
    dispatch({type: UPDATE_DELAY, payload: delay});
};

function countRegistrationDelay(from) {
    const passed = Date.now() - from;
    return passed < REGISTRATION_DELAY ? REGISTRATION_DELAY - passed : 0;
}

function presetOptions(options) {
    options = Object.assign({}, defaultOptions, options);

    return localeService.setLocale(options.locale).then(locale => {
        return Object.assign({}, defaultOptions, options, {locale});
    });
}

export const checkKey = () => (dispatch, getState) => {
    const { activation } = getState();
    const code = activation.codeInput;

    dispatch({ type: CHECK_KEY });
    dispatch(togglePreloader(true));

    return Promise.resolve().then(() => {
        if (!code) throw new CodeInvalidError(getMessage('errorMessageEmpty'));
        return apiService.getAccountInfo(code);
    }).then(account => {
        if (account.trial) {
            dispatch(setTrial({...account}));
        } else {
            dispatch({ type: ACTIVATE_PREMIUM_SUCCESS });
            dispatch(setPremium({ ...account, enabled: true }));
        }
        dispatch(togglePreloader());

        detectService.detectCompetitor()
          .then(data => {
              if (data.length > 0) {
                  dispatch({type: FOUND_COMPETITORS, payload: data});
                  dispatch(navActions.openCompetitors());
              } else {
                  getRandomServer(account).then(serverName => {
                      dispatch(setProxy(serverName));
                      if (localStorage.getItem('intro')) {
                          dispatch(openProxy());
                      } else {
                          dispatch(openInstructions());
                      }
                  });
              }
              dispatch(fetchProxies());
          })
        // dispatch(fetchProxies());

    }).catch(err => {
        dispatch({ type: ACTIVATE_PREMIUM_FAILURE, payload: err });
        dispatch(togglePreloader());
    });
};

function getRandomServer(acct) {
    const proxies = acct.trial ? apiService.getTrialProxies(acct.code) : apiService.getPremiumProxies(acct.code);
    return proxies.then(servers => {
        if (acct.trial) return servers.find(server => server.trial === true);
        return servers[Math.floor(Math.random() * servers.length)];
    })
}

export const disabledExtension = () => (dispatch, getState) => {
    const { competitor } = getState();
    const competitorID = competitor.map(el => el.id);

    dispatch(openProxy());
    dispatch({type: DISABLE_COMPETITORS});
    detectService.disabledAllExtension(competitorID);
}

export const selectFavorite = countryCode => (dispatch, getState) => {
    const { locations } = getState();
    const proxy = locations.favorites.includes(countryCode);
    let data = [];

    if (!proxy) {
        data = [...locations.favorites, countryCode];
    } else {
        data = locations.favorites.filter(item => item !== countryCode);
    }
    dispatch(saveFavorites(data));
};

export const saveFavorites = data => (dispatch, getState) => {
    dispatch({type: CHANGE_FAVORITES, payload: data});
    return storageService.setItem('favorites', data);

};

export const getIp = () => (dispatch, getState) => {
    dispatch({type: PENDING_IP});
    apiService.getIpInfo()
      .then(data => {
          dispatch({type: SET_IP_INFO, payload: data})
      })
}

// export const getPromo = (dispatch, getState) => {
//     apiService.getPromoDeal()
//       .then(data => {
//           dispatch({type: SET_PROMO_URL, payload: data});
//       })
// }