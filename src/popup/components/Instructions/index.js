import React, {useEffect, useState} from "react";
import './style.css'
// import data from "./data";
import {datacz,datade,dataen,dataes,datafr,datait,datapl,datapt,datatr,dataru,datanl} from "./data"
import closeSlide from '../../images/closeSlide.png'
export default function Instructions(props) {
    const { navActions,options } = props;
    const [slideIndex, setSlideIndex] = useState(1);
    const [leftArrowColor, setArrowColor] = useState('#D9D9D9');
    const [data, setData] = useState([]);

    useEffect(()=>{
        switch (options.locale) {
            case 'cz': return setData(datacz);
            case 'de': return setData(datade);
            case 'en': return setData(dataen);
            case 'es': return setData(dataes);
            case 'fr': return setData(datafr);
            case 'it': return setData(datait);
            case 'nl': return setData(datanl);
            case 'pl': return setData(datapl);
            case 'pt': return setData(datapt);
            case 'ru': return setData(dataru);
            case 'tr': return setData(datatr);
            default: return setData(dataen);
        }
    },[options.locale])

    const nextSlide = () => {
        if (slideIndex !== data.length) {
            setSlideIndex(slideIndex + 1)
            setArrowColor('#0079FB');
        } else if (slideIndex === data.length) {
            localStorage.setItem('intro', 'yes');
            navActions.openProxy();
        }
    }
    const prevSlide = () => {
        if (slideIndex !== 1) {
            setSlideIndex(slideIndex - 1);
            if(slideIndex===2) {
                setArrowColor('#D9D9D9');
            }
        }
    }
    const moveDot = index => {
        setSlideIndex(index);
        if(index === 1) {
            setArrowColor('#D9D9D9');
            return;
        }
        setArrowColor('#0079FB');
    }

    return (
        <div className='instructions'>

            {data.map((obj, index) => {
                return (
                    <div
                        key={obj.id}
                        className={slideIndex === index + 1 ? 'instructions__item active' : 'instructions__item'}
                        onClick={nextSlide}
                    >
                        <img src={obj.img} alt={''}/>
                    </div>
                )
            })}

            <button onClick={prevSlide} className={'instructions__arrow prev'}>
                <svg width="18" height="8" viewBox="0 0 18 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M17.9607 4.25076C17.8964 4.38347 17.764 4.50245 17.6246 4.5529C17.5511 4.57952 15.172 4.59113 9.7851 4.59113L2.05114 4.59113L3.38886 5.87198C4.83521 7.25684 4.81503 7.23198 4.75175 7.55415C4.71712 7.73049 4.49272 7.94482 4.30808 7.97788C3.96243 8.03983 4.04946 8.10891 1.96801 6.12059C-0.113438 4.13224 -0.0404758 4.21615 0.0222836 3.88352C0.0446053 3.76515 0.278236 3.5289 1.98651 1.89736C4.08953 -0.111165 3.99983 -0.0399027 4.34709 0.022295C4.52592 0.0543251 4.75628 0.277928 4.79065 0.452734C4.85224 0.766408 4.8689 0.746022 3.40803 2.14561L2.0526 3.44413L9.84178 3.45334L17.6309 3.46254L17.7345 3.53642C17.7915 3.57705 17.8711 3.65243 17.9113 3.70392C18.0041 3.82265 18.0299 4.10799 17.9607 4.25076Z" fill={leftArrowColor}/>
                </svg>
            </button>

            <div className="instructions__dots">
                {Array.from({length:4}).map((item,index)=>(
                    <div
                    onClick={()=>moveDot(index+1)}
                    className={slideIndex===index+1?'instructions__dot active':'instructions__dot'}
                    ></div>
                ))}
            </div>

            <button onClick={nextSlide} className={'instructions__arrow next'}>
                <svg width="18" height="8" viewBox="0 0 18 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0.0392981 4.25076C0.103633 4.38347 0.235979 4.50245 0.375378 4.5529C0.448941 4.57952 2.82798 4.59113 8.2149 4.59113L15.9489 4.59113L14.6111 5.87198C13.1648 7.25684 13.185 7.23198 13.2483 7.55415C13.2829 7.73049 13.5073 7.94482 13.6919 7.97788C14.0376 8.03983 13.9505 8.10891 16.032 6.12059C18.1134 4.13224 18.0405 4.21615 17.9777 3.88352C17.9554 3.76515 17.7218 3.5289 16.0135 1.89736C13.9105 -0.111165 14.0002 -0.0399027 13.6529 0.022295C13.4741 0.0543251 13.2437 0.277928 13.2094 0.452734C13.1478 0.766408 13.1311 0.746022 14.592 2.14561L15.9474 3.44413L8.15822 3.45334L0.369076 3.46254L0.265464 3.53642C0.208482 3.57705 0.128917 3.65243 0.0886653 3.70392C-0.00414205 3.82265 -0.0299135 4.10799 0.0392981 4.25076Z" fill="#0079FB"/>
                </svg>
            </button>
            <button onClick={()=>navActions.openProxy()} className={'instructions__close'}><img src={closeSlide} alt={'close'}/></button>
        </div>
    )
}