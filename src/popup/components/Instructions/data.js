import {v4 as uuidv4} from "uuid";


// тут сделать проверку какой сейчас язык, и в зависимости от этого загрузить интересующий меня кейс


import slideImg1cz from '../../images/instructions/cz/1cz.png'
import slideImg2cz from '../../images/instructions/cz/2cz.png'
import slideImg3cz from '../../images/instructions/cz/3cz.png'
import slideImg4cz from '../../images/instructions/cz/4cz.png'


import slideImg1de from '../../images/instructions/de/1de.png'
import slideImg2de from '../../images/instructions/de/2de.png'
import slideImg3de from '../../images/instructions/de/3de.png'
import slideImg4de from '../../images/instructions/de/4de.png'

import slideImg1en from '../../images/instructions/en/1en.png'
import slideImg2en from '../../images/instructions/en/2en.png'
import slideImg3en from '../../images/instructions/en/3en.png'
import slideImg4en from '../../images/instructions/en/4en.png'

import slideImg1es from '../../images/instructions/es/1es.png'
import slideImg2es from '../../images/instructions/es/2es.png'
import slideImg3es from '../../images/instructions/es/3es.png'
import slideImg4es from '../../images/instructions/es/4es.png'

import slideImg1fr from '../../images/instructions/fr/1fr.png'
import slideImg2fr from '../../images/instructions/fr/2fr.png'
import slideImg3fr from '../../images/instructions/fr/3fr.png'
import slideImg4fr from '../../images/instructions/fr/4fr.png'

import slideImg1it from '../../images/instructions/it/1it.png'
import slideImg2it from '../../images/instructions/it/2it.png'
import slideImg3it from '../../images/instructions/it/3it.png'
import slideImg4it from '../../images/instructions/it/4it.png'

import slideImg1nl from '../../images/instructions/nl/1nl.png'
import slideImg2nl from '../../images/instructions/nl/2nl.png'
import slideImg3nl from '../../images/instructions/nl/3nl.png'
import slideImg4nl from '../../images/instructions/nl/4nl.png'

import slideImg1pl from '../../images/instructions/pl/1pl.png'
import slideImg2pl from '../../images/instructions/pl/2pl.png'
import slideImg3pl from '../../images/instructions/pl/3pl.png'
import slideImg4pl from '../../images/instructions/pl/4pl.png'


import slideImg1pt from '../../images/instructions/pt/1pt.png'
import slideImg2pt from '../../images/instructions/pt/2pt.png'
import slideImg3pt from '../../images/instructions/pt/3pt.png'
import slideImg4pt from '../../images/instructions/pt/4pt.png'


import slideImg1ru from '../../images/instructions/ru/1ru.png'
import slideImg2ru from '../../images/instructions/ru/2ru.png'
import slideImg3ru from '../../images/instructions/ru/3ru.png'
import slideImg4ru from '../../images/instructions/ru/4ru.png'


import slideImg1tr from '../../images/instructions/tr/1tr.png'
import slideImg2tr from '../../images/instructions/tr/2tr.png'
import slideImg3tr from '../../images/instructions/tr/3tr.png'
import slideImg4tr from '../../images/instructions/tr/4tr.png'




export const datacz = [
    {
        id: uuidv4(),
        img: slideImg1cz
    },
    {
        id: uuidv4(),
        img: slideImg2cz
    },
    {
        id: uuidv4(),
        img: slideImg3cz
    },
    {
        id: uuidv4(),
        img: slideImg4cz
    },
];

export const datade = [
    {
        id: uuidv4(),
        img: slideImg1de
    },
    {
        id: uuidv4(),
        img: slideImg2de
    },
    {
        id: uuidv4(),
        img: slideImg3de
    },
    {
        id: uuidv4(),
        img: slideImg4de
    },
];

export const dataen = [
    {
        id: uuidv4(),
        img: slideImg1en
    },
    {
        id: uuidv4(),
        img: slideImg2en
    },
    {
        id: uuidv4(),
        img: slideImg3en
    },
    {
        id: uuidv4(),
        img: slideImg4en
    },
];

export const dataes = [
    {
        id: uuidv4(),
        img: slideImg1es
    },
    {
        id: uuidv4(),
        img: slideImg2es
    },
    {
        id: uuidv4(),
        img: slideImg3es
    },
    {
        id: uuidv4(),
        img: slideImg4es
    },
];

export const datafr = [
    {
        id: uuidv4(),
        img: slideImg1fr
    },
    {
        id: uuidv4(),
        img: slideImg2fr
    },
    {
        id: uuidv4(),
        img: slideImg3fr
    },
    {
        id: uuidv4(),
        img: slideImg4fr
    },
];

export const datait = [
    {
        id: uuidv4(),
        img: slideImg1it
    },
    {
        id: uuidv4(),
        img: slideImg2it
    },
    {
        id: uuidv4(),
        img: slideImg3it
    },
    {
        id: uuidv4(),
        img: slideImg4it
    },
];

export const datanl = [
    {
        id: uuidv4(),
        img: slideImg1nl
    },
    {
        id: uuidv4(),
        img: slideImg2nl
    },
    {
        id: uuidv4(),
        img: slideImg3nl
    },
    {
        id: uuidv4(),
        img: slideImg4nl
    },
];

export const datapl = [
    {
        id: uuidv4(),
        img: slideImg1pl
    },
    {
        id: uuidv4(),
        img: slideImg2pl
    },
    {
        id: uuidv4(),
        img: slideImg3pl
    },
    {
        id: uuidv4(),
        img: slideImg4pl
    },
];
export const datapt = [
    {
        id: uuidv4(),
        img: slideImg1pt
    },
    {
        id: uuidv4(),
        img: slideImg2pt
    },
    {
        id: uuidv4(),
        img: slideImg3pt
    },
    {
        id: uuidv4(),
        img: slideImg4pt
    },
];
export const dataru = [
    {
        id: uuidv4(),
        img: slideImg1ru
    },
    {
        id: uuidv4(),
        img: slideImg2ru
    },
    {
        id: uuidv4(),
        img: slideImg3ru
    },
    {
        id: uuidv4(),
        img: slideImg4ru
    },
];

export const datatr = [
    {
        id: uuidv4(),
        img: slideImg1tr
    },
    {
        id: uuidv4(),
        img: slideImg2tr
    },
    {
        id: uuidv4(),
        img: slideImg3tr
    },
    {
        id: uuidv4(),
        img: slideImg4tr
    },
];

// export default data;