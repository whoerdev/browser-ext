
import React from 'react';
import Localize from '../../containers/Localize';
import localeService from '../../../services/locale';
import HelpCenterSVG from '../../images/shape-8.svg';


const PremiumFooter = (props) => {
    const {account, onLogout} = props;
    const planDays = +account.plan || 30;
    let daysLeft = Math.floor((account.expires - Date.now()) / 24 / 60 / 60 / 1000) || 0;
    let progressRate;

    if (daysLeft <= 0) {
        daysLeft = 0;
        progressRate = 1;
    }
    else if (daysLeft > planDays) {
        progressRate = 0;
    }
    else {
        progressRate = 1 - daysLeft / planDays;
    }

    return(
        <div>
            <div class="you-are-premium-user-message">
                <Localize value="premiumStatus" substitution={localeService.getPluralMessage('daysLeft', daysLeft)}/>
            </div>
            <div class="premium-progress-bar">
                <div class="premium-left-progress-bar" style={{width: progressRate * 281}}>
                </div>
            </div>
            <div style={{textAlign: 'center'}}>
                <div class="logout">
                    <div style={{lineHeight: '0'}}>
                        <img src={HelpCenterSVG} class="logout-image" alt=""/>
                    </div>
                    <div class="wrapper-logout-text">
                        <a class="logout-link" onClick={onLogout}>
                            <Localize value="logoutLabel"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    );
};


export default PremiumFooter;
