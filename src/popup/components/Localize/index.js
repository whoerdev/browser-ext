
import React/*, { Component }*/ from 'react';
import localeService from '../../../services/locale';


const Localize = (params) => {
    const {/*locale,*/ value, substitution, count} = params;
    const val = typeof value === 'string' && value.trim();
    let text = '';

    if (val) {
        if ('count' in params) {
            text = localeService.getPluralMessage(val, [count].concat(substitution));
        }
        else {
            text = localeService.getMessage(val, substitution);
        }
    }

    return (
        <span>{text}</span>
    );
};

export default Localize;
