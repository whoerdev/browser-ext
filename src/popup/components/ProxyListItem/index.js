
import React, {Component} from 'react';
import './styles.css';
import Localize from "../../containers/Localize";
import connectedCheckImage from "../../images/proxy-conn-check.svg";
import star from "../../images/star.svg";
import staryellow from '../../images/star-yellow.svg'
import locked from "../../images/locked.svg";
import time from "../../images/time.svg";

const BACKEND_BASE_URL = process.env.WHOER_BACKEND_BASE_URL;


class ProxyListItem extends Component {

    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);

        this.state = {
            flagImage: ''
        };
    }
    componentDidMount() {
        const { proxy } = this.props;

        // import('../../images/flags/' + proxy.countryCode + '.png').then(image => {
        //     this.setState({flagImage: image})
        // }).catch(err => {
        //     this.setState({flagImage: `${BACKEND_BASE_URL}/images/flagsvpn/${proxy.countryCode}.png`});
            this.setState({flagImage: `${BACKEND_BASE_URL}/images/map-pins/pin_${proxy.countryCode}.svg`});
        // });
    }

    render() {
        const { state, props } = this;
        const { proxy, account, favorites } = props;
        const itemClasses = ['proxy-item'];
        let proxyStatus;
        let proxyStatusImg;

        if (proxy.trial) {
            itemClasses.push('proxy-item--trial');
            // proxyStatus = 'locationsItemTrial';
        }
        else {
            itemClasses.push('proxy-item--premium');
            // proxyStatus = 'locationsItemPremium';
        }

        if (proxy.selected) {
            itemClasses.push('proxy-item--connected');
            // proxyStatus = 'locationsItemConnected';
        }

        if (account && !proxy.trial && !account.trial && favorites.includes(proxy.countryCode)) {
            proxyStatusImg = staryellow;
            proxyStatus = 'locationsItemPremium';
        } else if (account && !proxy.trial && !account.trial) {
            proxyStatusImg = star;
            proxyStatus = 'locationsItemPremium';
        } else if (account && !proxy.trial && account.trial) {
            proxyStatusImg = locked;
            proxyStatus = 'locationsItemPremium';
        } else {
            proxyStatusImg = time;
            proxyStatus = 'locationsItemTrial';
        }
        console.log(!proxy.trial, !account.trial, favorites.includes(proxy.countryCode));


        return (
            <li className={itemClasses.join(' ')} onClick={this.onClick}>
                <img src={state.flagImage} className="country-image" alt="" />
                <span className="item-country-name">
                    {proxy.countryName}
                </span>
                <span className="proxy-item__status">
                    {proxy.selected ? <img src={connectedCheckImage} alt=""/> : ''}
                    <Localize value={proxyStatus}/>
                </span>
                <span className="proxy-item__connect">
                    <Localize value="locationsItemConnect"/>
                </span>
                <img className="proxy-item__status-img" src={proxyStatusImg} alt="star"/>
            </li>
        );
    }

    onClick(event) {
        const { props } = this;
        const { proxy } = props;
        const target = event.target;

        if (target.className === 'proxy-item__status-img' && !props.account.trial) {
            console.log('click on icon');
            props.onChangeFavorite(proxy.countryCode);
        } else {
            if (props.onSelect) {
                props.onSelect(proxy.countryCode);
            }

        }
    }
}

export default ProxyListItem;
