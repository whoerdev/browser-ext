
import React, { Component } from 'react';
import Page from '../../containers/Page';
import './style.css';
import coins from '../../images/coins_outline.svg';
import download from '../../images/download_outline.svg';
import headphones from '../../images/headphones_outline.svg';
import wifi from '../../images/wifi_outline.svg'
import headSubs from '../../images/headSubscription.png';
import localeService from "../../../services/locale";

class Subscription extends Component {
  render() {
    // const { account, delay, competitors, commonActions, navActions, proxyEnabled } = this.props;

    return (
      <Page
        optionsAvailable={false}
        isClose={true}
        title={'noneTexts'}
        className={'subscription'}
      >
        <div className={'header__img'}><img src={headSubs} alt=""/></div>
        <h2 className={'subscription__title'}>{localeService.getMessage('subscriotionTitle')}</h2>
        <ul className={'subscription__list'}>
          <li className={'subscription__list-el'}>
            <img alt={''} className={'subscription__list-img'} src={coins}/>
            <span className={'subscription__list-text'}>{localeService.getMessage('subscriotionListSecure')}</span>
          </li>
          <li className={'subscription__list-el'}>
            <img alt={''} className={'subscription__list-img'} src={wifi}/>
            <span className={'subscription__list-text'}>{localeService.getMessage('subscriotionListSpeed')}</span>
          </li>
          <li className={'subscription__list-el'}>
            <img alt={''} className={'subscription__list-img'} src={headphones}/>
            <span className={'subscription__list-text'}>{localeService.getMessage('subscriotionListSupport')}</span>
          </li>
          <li className={'subscription__list-el'}>
            <img alt={''} className={'subscription__list-img'} src={download}/>
            <span className={'subscription__list-text'}>{localeService.getMessage('subscriotionListMoney')}</span>
          </li>
        </ul>

        {/*<p className={'subscription__description'}>{localeService.getMessage('subscriotionDescription')}</p>*/}
        <a className="subscription__link"
           href='https://whoer.net/vpn/buy'
           target='_blank'
           rel='noopener noreferrer'
        >
          {localeService.getMessage('subscriotionButton')}
        </a>
      </Page>
    );
  }

}

export default Subscription;
