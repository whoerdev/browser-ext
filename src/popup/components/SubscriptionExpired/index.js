
import React, { Component } from 'react';
import Page from '../../containers/Page';
import './style.css';
import localeService from "../../../services/locale";
import Icon from '../../images/notification.svg';

class SubscriptionExpired extends Component {
  render() {
    // const { account, delay, competitors, commonActions, navActions, proxyEnabled } = this.props;


    return (
      <Page
        needLogo={true}
        className={'subscription-expired'}
      >
        <img
          src={Icon}
          alt=""
          className={'subscription-expired__img'}
        />
        <h2 className={'subscription-expired__title'}>{localeService.getMessage('subscriptionExpiredTitle')}</h2>
        <p className={'subscription-expired__description'}>{localeService.getMessage('subscriptionExpiredDescription')}</p>
        <a className="subscription__link subscription-expired__link "
           href='https://whoer.net/vpn/buy'
           target='_blank'
           rel='noopener noreferrer'
        >
          {localeService.getMessage('subscriptionExpiredButton')}
        </a>
      </Page>
    );
  }

}

export default SubscriptionExpired;
