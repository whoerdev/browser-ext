import React from 'react';
import Localize from '../../containers/Localize';
import localeService from '../../../services/locale';
import Time from '../../images/low-time.svg';
import './style.css';


const ImproveAccountBanner = (props) => {
  const {account} = props;
  let daysLeft = Math.floor((account.expires - Date.now()) / 24 / 60 / 60 / 1000) || 0;
  let isNotTrial = account && !account.trial;
  let shortcutSubstitution=localeService.getPluralMessage('daysLeft', daysLeft)
  return(
    <div className={isNotTrial ? 'improve-account-banner' : 'improve-account-banner improve-account-banner--trial'}>
      {isNotTrial && daysLeft < 9 &&
          <img src={Time} alt="Low time"/>
      }
      <div>
          <p className="improve-account-banner__title">
            {isNotTrial ?
                <Localize value="premiumStatusShort"
                    substitution={shortcutSubstitution.length>15? shortcutSubstitution.slice(0,15)+'...': shortcutSubstitution}/>
                :
                <Localize value="upgradeToPremium"/>
            }
          </p>
          <a
            className="improve-account-banner__link"
            href={isNotTrial && daysLeft < 9 ? localeService.getLinkWithPrefix('vpn/buy?coupon=VPN15DISCOUNT') : localeService.getLinkWithPrefix('vpn/buy')}
            target='_blank'
            rel='noopener noreferrer'
          >
            <Localize value={isNotTrial ? 'extendTranslate' : 'upgradeTranslate'}/>
          </a>
      </div>
    </div>
  );
};

export default ImproveAccountBanner;
