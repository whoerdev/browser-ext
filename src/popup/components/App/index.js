
import React, { Component } from 'react';
import Proxy from '../../containers/Proxy';
import Proxies from '../../containers/Proxies';
import Activation from '../../containers/Activation';
import Options from '../../containers/Options';
import Error from '../../containers/Error';
import Login from "../../containers/Login";
import Registration from "../../containers/Registration";
import StartPage from "../../containers/Start";
import RegistrationNotification from "../../containers/RegistrationNotification";
import storageService from '../../../services/storage';
import messageService from '../../../services/message';
import './styles.css';
import Competitor from "../../containers/Competitor";
import Subscription from "../Subscription";
import SubscriptionLowTime from "../../containers/SubscriptionLowTime";
import SubscriptionExpired from "../SubscriptionExpired"
import Instructions from "../../containers/Instructions";


class App extends Component {

    componentDidMount() {
        const { commonActions } = this.props;

        storageService.addChangeListener((items => {
            const data = {};
            Object.keys(items).forEach(key => data[key] = items[key].newValue);
            commonActions.updateState(data);
        }));

        messageService.addCustomListener('registrationDelay', (data) => {
            commonActions.updateDelay(data);
        });

        messageService.addCustomListener('proxyError', (data) => {
            commonActions.rejectProxy(data);
        });

        commonActions.initApp();
    }

    render() {
        const { page, error } = this.props;

        if (error) {
            return <Error/>;
        }

        switch (page) {
            case 'startPage': return <StartPage/>;
            case 'login': return <Login/>;
            case 'registration': return <Registration/>;
            case 'registrationNotification': return <RegistrationNotification/>;
            case 'proxy': return <Proxy/>;
            case 'proxies': return <Proxies/>;
            case 'activation': return <Activation/>;
            case 'options': return <Options/>;
            case 'competitors': return <Competitor/>;
            case 'subscription': return <Subscription/>
            case 'subscriptionLowTime': return <SubscriptionLowTime/>
            case 'subscriptionExpired': return <SubscriptionExpired/>
            case 'instructions': return <Instructions/>
            default: return <div/>;
        }
    }
}

export default App;
