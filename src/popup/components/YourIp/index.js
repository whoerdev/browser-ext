import React, {Component} from "react";
import localeService from "../../../services/locale";
import './style.css';
import Preloader from '../../images/three-dots.svg';
const BACKEND_BASE_URL = process.env.WHOER_BACKEND_BASE_URL;

class YourIp extends Component {

  render() {
    // const { props } = this;
    // const { navActions } = this.props;
    const { ipInfo } = this.props;
    return (
      <div className={'your-ip'}>
        <p className={'your-ip__title'}>{localeService.getMessage('yourIp')}:</p>
          {ipInfo ?
            <div className={'your-ip__ip-wrapper'}>
              <img
              className={'your-ip__flag'}
              src={`${BACKEND_BASE_URL}/images/flags/${ipInfo.countryCode}.svg`}
              alt={''}/>
              <span className={'your-ip__ip'}>{ipInfo.ip}</span></div>
            :
            <img alt={''} src={Preloader} width={60} height={19}/>
          }
      </div>
    );
  }
}

export default YourIp