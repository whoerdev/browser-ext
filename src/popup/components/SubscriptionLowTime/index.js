import React, { Component } from 'react';
import Page from '../../containers/Page';
import localeService from "../../../services/locale";
import headSubs from '../../images/low-time-circle.svg';
import './style.css';
import Localize from "../../containers/Localize";

class SubscriptionLowTime extends Component {
  render() {
    const {account} = this.props;
    let daysLeft = Math.floor((account.expires - Date.now()) / 24 / 60 / 60 / 1000) || 0;

    return (
      <Page
        optionsAvailable={false}
        isClose={true}
        isCloseColorBlack={true}
        title={'noneTexts'}
        className={'subscription-low-time'}
      >
        <div className={'header__img'}><img src={headSubs} alt=""/></div>
        <h2 className={'subscription-low-time__title'}>
          {<Localize value="subscriptionLowTimeTitle" substitution={daysLeft}/>}
        </h2>
        <p>{<Localize value="subscriptionLowTimeDescription"/>}</p>
        <a className="subscription-low-time__link"
           href={localeService.getLinkWithPrefix('vpn/buy?coupon=VPN15DISCOUNT')}
           target='_blank'
           rel='noopener noreferrer'
        >
          {localeService.getMessage('extendTranslate')}
        </a>
      </Page>
    );
  }

}

export default SubscriptionLowTime;
