
import React, { Component } from 'react';
import Page from '../../containers/Page';
import Timer from '../../components/Timer';
// import ImproveAccountBanner from '../ImproveAccountBanner'
//import Localize from '../../containers/Localize';
// import './styles.css';
import './style.css';
// import {disabledExtension} from "../../actions/common";
import localeService from "../../../services/locale";

class Competitor extends Component {
  render() {
    const { delay, competitors, commonActions } = this.props;

    const competitorsList = competitors.map(competitor => {
      return (
        <li
          className={'competitor__element'}
          key={competitor.id}>
          <img
            className={'competitor__element-icon'}
            src={competitor.icon}
            height="16px"
            alt={"icon " + competitor.name}/>
          <span
            className={'competitor__element-name'}
          >
            {competitor.name}
          </span>
        </li>
      )
    })
    // console.log('competitors state:', competitors)

    if (delay) {
      return <Timer delay={delay}/>
    }

    return (
      <Page
        needLogo={true}
      >
        <div className={'competitor'}>
          <p className={'competitor__title'}>
            {localeService.getMessage('competitorTitle')}
          </p>
          <ul className={'competitor__list'}>
            {competitorsList}
          </ul>
          <button
            className={'competitor__disabled-button'}
            onClick={commonActions.disabledExtension}
          >
            {localeService.getMessage('competitorButton')}
          </button>
        </div>
      </Page>
    );
  }

}

export default Competitor;
