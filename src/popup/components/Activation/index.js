
import React, { Component } from 'react';
import { openTab } from '../../../services/browser';
import Page from  '../../containers/Page';
import PremiumButton from '../PremiumButton';
import BackButton from '../BackButton';
import Localize from '../../containers/Localize';
import localeService from '../../../services/locale';
import CheckSVG from '../../images/rectangle-4-copy.svg';
import ErrorSVG from '../../images/deny.svg';
import './styles.css';


const GET_PREMIUM_URL = process.env.WHOER_GET_PREMIUM_URL;


class Activation extends Component {

    constructor(props) {
        super(props);

        this.onCodeChange = this.onCodeChange.bind(this);
        this.onCodeSubmit = this.onCodeSubmit.bind(this);
        this.onCodeBlur = this.onCodeBlur.bind(this);
    }

    render() {
        const { props } = this;
        const { activation/*, commonActions*/, navActions } = props;
        const statusDefined = activation.valid || activation.error;
        const statusImage = activation.valid ? CheckSVG : ErrorSVG;

        return (
            <Page>
                <div className="premium-input-container">
                    <div className="text-above-input"><Localize value="activationCodeTitle"/></div>
                    <div className="input-box">
                        <form onSubmit={this.onCodeSubmit} onBlur={this.onCodeBlur}>
                            <input
                                type="text"
                                placeholder={localeService.getMessage('activationCodePlaceholder')}
                                className="premium-input"
                                name=""
                                value={activation.codeInput}
                                onChange={this.onCodeChange}
                            />
                        </form>
                        {statusDefined ? <img className="input-image" src={statusImage} alt=""/> : null}
                    </div>
                </div>
                <div className="where-to-get-a-passcode">
                    <Localize value="activationDescriptionTitle"/>
                </div>
                <div className="lorem-ipsum">
                    <Localize value="activationDescriptionText"/>
                </div>
                <PremiumButton
                    label="registrationButtonLabel"
                    onClick={() => openTab(GET_PREMIUM_URL)}
                />
                <div className="activation-back-wrapper">
                    <BackButton onClick={navActions.backPage}/>
                </div>
            </Page>
        );
    }

    onCodeChange(e) {
        const { commonActions } = this.props;
        const value = e.target.value;

        commonActions.changeActivationCode(value);
        if (value.length === 13)
            commonActions.checkPremiumInput();
    }

    onCodeSubmit(e) {
        const { commonActions } = this.props;

        e.preventDefault();
        commonActions.activatePremium();
    }

    onCodeBlur(e) {
        const { commonActions } = this.props;

        e.preventDefault();
        commonActions.checkPremiumInput();
    }
}

export default Activation;
