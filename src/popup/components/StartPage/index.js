import React, {Component} from "react";
// import localeService from "../../../services/locale";
import './style.css';
import Logo from '../../images/logo.svg';

class StartPage extends Component {


  render() {
    const {children} = this.props;

    return(
      <div className="start-page">
        <img
          className="start-page-logo"
          src={Logo}
          style={{height: 70, width: 70 }}
          alt="logo"
        />
        {children}
      </div>
    );
  }
}
export default StartPage