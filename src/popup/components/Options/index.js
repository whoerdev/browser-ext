import React, {Component} from 'react';
import Page from '../../containers/Page';
// import Divider from '../Divider';
import Switch from '../Switch';
// import BackButton from '../BackButton';
import Localize from '../../containers/Localize';
import QuestionImage from '../../images/question-circle.svg'
import QuestionImageLink from '../../images/question.svg'
import LogOutImage from '../../images/log-out.svg'
import localeService from '../../../services/locale';
import Select from '../Select';
import './styles.css';
// import {openRegistration} from "../../actions/navigation";
// import {resetTrial, setTrial} from "../../actions/common";
// import ImproveAccountBanner from "../ImproveAccountBanner";


//const BACKEND_BASE_URL = process.env.WHOER_BACKEND_BASE_URL;


class Options extends Component {

    constructor(props) {
        super(props);

        this.renderLanguageSelectOption = this.renderLanguageSelectOption.bind(this);
        this.onLogOut = this.onLogOut.bind(this);
    }

    render() {
        const {props} = this;
        const {options, commonActions, navActions} = props;
        const {account} = this.props;
        let isNotTrial = account && !account.trial;

        return (
          <Page
            className="page-options"
            optionsAvailable={false}
            needBackButton={true}
            title="settingsPageTitle"
          >
              <div className="options-list">
                  <div className="options-list-item options-list-item--language">
                      <div className="options-list-item__label">
                          <Localize value="languageOptionLabel"/>
                      </div>
                      <div className="options-list-item__control">
                          {this.renderLanguage()}
                      </div>
                  </div>
                  <div className="options-list-item">
                      <div className="options-list-item__block">
                          <Localize value="webRtcLeak"/>
                          <a
                            href={`https://whoer.net/blog/${localeService.getMessage('webrtcLink')}`}
                            target='_blank'
                            rel='noopener noreferrer'
                            className="options-list-item__question-link"
                          >
                              <img
                                alt={''}
                                src={QuestionImageLink}
                                style={{height: 17, width: 17}}
                              />
                          </a>
                      </div>
                      <div className="options-list-item__control">
                          <Switch
                            checked={options.preventWebRTCLeak}
                            onChange={val => commonActions.setOption('preventWebRTCLeak', val)}
                          />
                      </div>
                  </div>
                  <div className="options-list-item">
                      <div className="options-list-item__block">
                          {isNotTrial ?
                            <Localize value="optionExtendSubscription"/> :
                            <Localize value="optionGetPremium"/>
                          }
                      </div>
                      <div className="options-list-item__control">
                          <a
                            className="options-list-item__buy"
                            href={localeService.getLinkWithPrefix('vpn/buy')}
                            target='_blank'
                            rel='noopener noreferrer'
                          >
                              <Localize value={isNotTrial ? 'extendTranslate' : 'buyTranslate'}/>
                          </a>
                      </div>
                  </div>
                  {/*<div className="options-list-item">*/}
                  {/*    <div className="options-list-item__label">*/}
                  {/*        <Localize value="disableFlashOptionLabel"/>*/}
                  {/*    </div>*/}
                  {/*    <div className="options-list-item__control">*/}
                  {/*        <Switch*/}
                  {/*            checked={options.disableFlash}*/}
                  {/*            onChange={val => commonActions.setOption('disableFlash', val)}*/}
                  {/*        />*/}
                  {/*    </div>*/}
                  {/*</div>*/}
                  {/*<div className="options-list-item">*/}
                  {/*    <div className="options-list-item__label">*/}
                  {/*        /!*<Localize value="disableFlashOptionLabel"/>*!/*/}
                  {/*        Extend my subscription*/}
                  {/*    </div>*/}
                  {/*    <div className="options-list-item__control">*/}
                  {/*        <a className="options-list-item__buy"*/}
                  {/*           href='https://whoer.net/vpn/buy'*/}
                  {/*           target='_blank'*/}
                  {/*           rel='noopener noreferrer'*/}
                  {/*        >*/}
                  {/*            Extend*/}
                  {/*        </a>*/}
                  {/*    </div>*/}
                  {/*</div>*/}



                  <div className={'options-list-item options-proxy-instructions-open'}
                       onClick={() => navActions.openInstructions()}>
                      {localeService.getMessage('proxyInstructions')}
                      <div className="options-list-item__control">
                          <img
                            alt={''}
                            src={QuestionImage}
                            style={{height: 20, width: 20}}
                          />
                      </div>
                  </div>


                  <a

                    href={localeService.getLinkWithPrefix('?support_open=1')}
                    target='_blank'
                    rel='noopener noreferrer'
                    style={{textDecoration: 'none'}}
                  >
                      <div className="options-list-item">
                          <div className="options-list-item__label">
                              {localeService.getMessage('helpLinkLabel')}
                          </div>
                          <div className="options-list-item__control">
                              <img
                                alt={''}
                                src={QuestionImage}
                                style={{height: 20, width: 20}}
                              />
                          </div>
                      </div>

                  </a>

                  <div className="options-list-item" style={{cursor: 'pointer'}} onClick={this.onLogOut}>
                      <div className="options-list-item__label">
                          {/*<Localize value="disableFlashOptionLabel"/>*/}
                          <Localize value="logoutLabel"/>
                      </div>
                      <div className="options-list-item__control">
                          <img
                            alt={''}
                            className="options-list-item__logout"
                            src={LogOutImage}
                            style={{height: 20, width: 20}}
                            // onClick={this.onLogOut}
                          />
                      </div>
                  </div>
                  {/*<div className="options-list-item">*/}
                  {/*<div className="options-list-item__label">*/}
                  {/*<Localize value="adjustTimezoneOptionLabel"/>*/}
                  {/*</div>*/}
                  {/*<div className="options-list-item__control">*/}
                  {/*<Switch*/}
                  {/*checked={options.adjustTimezone}*/}
                  {/*onChange={val => commonActions.setOption('adjustTimezone', val)}*/}
                  {/*/>*/}
                  {/*</div>*/}
                  {/*</div>*/}
              </div>


              {/*<ImproveAccountBanner account={account}/>*/}
              {/*<Divider/>*/}
              {/*<BackButton onClick={navActions.backPage}/>*/}
          </Page>
        );
    }

    onLogOut() {
        const {commonActions, navActions} = this.props;
        commonActions.deactivatePremium();
        commonActions.setTrial();
        navActions.openLogin();
    }

    renderLanguage() {
        const {locales, options, commonActions} = this.props;

        return (
          <Select
            name="locale-select"
            value={options.locale}
            onChange={val => commonActions.setOption('locale', val)}
            valueKey="code"
            labelKey="name"
            items={locales}
            valueContent={this.renderLanguageSelectOption}
            optionContent={this.renderLanguageSelectOption}
          />
        );
    }

    renderLanguageSelectOption({code, name, image}) {

        return (
          <div className="locale-option-select-item" key={code}>
              {/*<img className="locale-option-select-item__image" src={BACKEND_BASE_URL + image}/>*/}
              <span className="locale-option-select-item__name">{name}</span>
          </div>
        );
    }
}

export default Options;
