
import React, {Component} from 'react';
import Page from '../../containers/Page';
import Timer from '../../components/Timer';
// import ProxySelectInvitation from '../ProxySelectInvitation';
import ProxySelection from '../ProxySelection';
// import PremiumFooter from '../PremiumFooter';
// import PremiumButton from '../PremiumButton';
// import ImproveAccountBanner from '../ImproveAccountBanner'
import SwitchBig from '../SwitchBig';
//import Localize from '../../containers/Localize';
import './styles.css';
import FoxNonHidden from '../../images/fox-non-hidden.svg';
import FoxHidden from '../../images/fox-hidden.svg';
import localeService from "../../../services/locale";
import YourIp from "../YourIp";
// import {getIp2} from "../../actions/common";
// import PromoDeal from "../PromoDeal";

import ImproveAccountBanner from "../ImproveAccountBanner";

class Proxy extends Component {
    render() {
        const { account, proxy, delay, commonActions, navActions, proxyEnabled, ipInfo, firstStartBrowser } = this.props;
        if (delay) {
            return <Timer delay={delay}/>
        }
        console.log('firstStartBrowser: ', firstStartBrowser);
        const daysLeft = Math.floor((account.expires - Date.now()) / 24 / 60 / 60 / 1000) || 0;
        if(firstStartBrowser.firstStartBrowser && !account.trial && daysLeft < 9) {
            navActions.openSubscriptionLowTime();
        }
        return (
            <Page
              needLogo={true}
              className={'page-proxy'}
            >
                <div className='proxy-fox-circle-out'>
                    <div className='proxy-fox-circle-in'>
                        <img
                            alt={''}
                            className='proxy-fox'
                            src={proxyEnabled ? FoxHidden : FoxNonHidden}
                        />
                    </div>
                </div>
                { account && account.trial?
                  <div className='proxy-account-status'>
                      {localeService.getMessage('proxyFreeAccount')}
                      <span className='proxy-account-status__speed'>
                        {localeService.getMessage('proxySpeedLimited')}
                    </span>
                  </div>
                  :
                  <YourIp
                    ipInfo={ipInfo}
                    proxyEnabled={proxyEnabled}
                  />
                }
                <div className="proxy-switcher">
                    {firstStartBrowser.firstStartBrowser && account.trial ?
                      <SwitchBig checked={proxyEnabled} onChange={navActions.openSubscription} ipInfo={ipInfo}/>
                       :
                      <SwitchBig checked={proxyEnabled} onChange={commonActions.toggleProxy} ipInfo={ipInfo}/>
                    }
                </div>
                <div className="proxy-location">
                    <p className="proxy-location__header">
                        <span className="proxy-location__title">
                            {localeService.getMessage('proxyLocation')}:
                        </span>
                        <span className="proxy-location__divider"/>
                    </p>
                </div>

                <ProxySelection
                    proxy={proxy}
                    account={account}
                    onClick={navActions.openProxies}
                />
                <ImproveAccountBanner account={account}/>
            </Page>
        );
    }
}

export default Proxy;