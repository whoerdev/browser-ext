import React, {Component} from "react";
import localeService from "../../../services/locale";
import './style.css';
import StartPage from "../StartPage";
// import {checkKey, resetError} from "../../actions/common";
import ovalPreloader from "../../images/oval-preloader.svg";

class Login extends Component{
  constructor(props) {
    super(props);
    this.state = {
      code: '',
    }
    this.onCodeChange = this.onCodeChange.bind(this);
    this.onCodeSubmit = this.onCodeSubmit.bind(this);
  }
  render() {

    const { state } = this;
    const { navActions, commonActions, login } = this.props;
    // console.log(`error`,login);
    // console.log(`error`,test);

    return (
      <StartPage>
        <h2
          className={`start-page-greeting start-page-greeting--login start-page-greeting--login-` + localeService.getLocale()}>
          {localeService.getMessage('loginTitle')}
        </h2>
        <form
          className="start-page-form"
          onSubmit={this.onCodeSubmit}
        >
          <input
            type="text"
            className="start-page-input start-page-input--login"
            name=""
            placeholder={localeService.getMessage('inputCode')}
            onChange={this.onCodeChange}
            onClick={commonActions.resetError}
            value={state.code}
          />
          {
            login.error ? <p className="start-page-error start-page-error--login">{login.error.message}</p> : null
          }
          <label className={'start-page-label'}>
            { login.preloader ?

              <img alt={''} className={'start-page-preloader'} src={ovalPreloader}/>
              :
              <input
                type="submit"
                className='start-page-submit'
                value={localeService.getMessage('loginButton')}
              />
            }
          </label>
        </form>
        <p className="start-page-recovery">
          {localeService.getMessage('loginForgotCode')}&nbsp;
          <a target='_blank' rel='noopener noreferrer' href={localeService.getLinkWithPrefix('vpn/recovery')}>
            {localeService.getMessage('loginForgotCodeLink')}
          </a>
        </p>
        <p className="start-page-hint">
          {localeService.getMessage('loginHint')}&nbsp;
          <span onClick={navActions.openRegistration}>
            {localeService.getMessage('loginHintLink')}
          </span>
        </p>
      </StartPage>
    )
  }

  onCodeChange(e) {
    const { commonActions } = this.props;
    const value = e.target.value;

    this.setState({code: value});

    commonActions.changeActivationCode(value);
    // if (value.length === 13)
    //   commonActions.checkPremiumInput();
  }

  onCodeSubmit(e) {
    const { commonActions } = this.props;

    e.preventDefault();
    commonActions.checkKey();
  }
}
export default Login