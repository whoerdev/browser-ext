
import React, { Component } from 'react';
import ProxyListItem from '../ProxyListItem';
import localeService from "../../../services/locale";


class ProxyList extends Component {

    render() {
        const { props } = this;
        if(props.proxies.length > 0) {
            return (
                <ul className="proxy-list">
                    { this.proxyFilter().map(proxy => {
                        return (
                          <ProxyListItem
                            key={proxy.countryCode}
                            proxy={proxy}
                            onSelect={props.onSelect}
                            account={props.account}
                            onChangeFavorite={props.onChangeFavorite}
                            favorites={props.favorites}
                        />
                        )
                    })}
                </ul>
            );
        } else {
            return (
                <div>{localeService.getMessage('nothingFound')}</div>
            )
        }

    }

    proxyFilter() {
        const {props} = this;
        if (props.isFavorites) return props.proxies.filter(proxy => props.favorites.includes(proxy.countryCode));
        if (props.isTrial) return props.proxies.filter(proxy => proxy.trial);
        return props.proxies.filter(proxy => !proxy.trial);
    }
}


export default ProxyList;
