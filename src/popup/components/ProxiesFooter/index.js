
import React/*, { Component }*/ from 'react';
import Localize from '../../containers/Localize';
import HelpCenterImage from '../../images/shape-3.svg';
import GoPremiumImage from '../../images/shape-5.svg';


const ProxyListFooter = (props) => (
    <div class="footer">
        <div class="help-center">
            <div class="wrapper-help-center-image">
                <img class="help-center-image" src={HelpCenterImage} alt=""/>
            </div>
            <div class="wrapper-help-center-text">
                <a class="help-center-link" onClick={props.onHelpClick}>
                    <div class="help-center-text"><Localize value="helpLinkLabel"/></div>
                </a>
            </div>
        </div>
        <div class="go-premium">
            <div class="wrapper-go-premium-image">
                <img class="go-premium-image" src={GoPremiumImage} alt=""/>
            </div>
            <div class="wrapper-go-premium-text">
                <a class="go-premium-link go-premium-text" onClick={props.onPremiumClick}>
                    <div class="go-premium-text"><Localize value="premiumLinkLabel"/></div>
                </a>
            </div>
        </div>
    </div>
);

export default ProxyListFooter;
