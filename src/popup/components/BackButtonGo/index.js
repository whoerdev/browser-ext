
import React from 'react';
import BackImage from '../../images/back.svg';
import Localize from '../../containers/Localize';

const BackButtonGo = (props) => {

  return (
      <div className="error-back" onClick={props.onClick}>
              <img
                src={BackImage}
                className="error-back__image"
                alt=""/>
            <Localize value='backLabel' />
      </div>
  );
};

export default BackButtonGo;
