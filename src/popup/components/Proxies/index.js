import React, { Component } from 'react';
// import { openTab } from '../../../services/browser';
import Page from '../../containers/Page';
// import Divider from '../Divider';
import ProxyList from '../ProxyList';
// import ProxiesFooter from '../ProxiesFooter';
import localeService from '../../../services/locale';
import SearchSVG from '../../images/search-location.svg';
import './styles.css';
// import {selectFavorite} from "../../actions/common";
// import ProxyListFavorites from "../ProxyListFavorites";
import ProxiesTrialBanner from "../ProxiesTrialBanner";


// const HELP_CENTER_URL = process.env.WHOER_HELP_CENTER_URL;


class Proxies extends Component {

    constructor(props) {
        super(props);

        this.onFilterChange = this.onFilterChange.bind(this);
    }

    render() {
        const { props } = this;
        const { commonActions } = props;
        console.log('from proxies:', props.favorites);
        return (
            <Page
              className="proxy-list-page"
              title="locationsPageTitle"
              needBackButton={true}
            >
                <div className="proxy-list-container">
                    <div className="search-proxy-input-box">
                        <img src={SearchSVG} className="search-proxy-input-image" alt=""/>
                        <input
                            type="text"
                            value={props.filter}
                            onChange={this.onFilterChange}
                            placeholder={localeService.getMessage('locationsSearchPlaceholder')}
                            className="search-proxy-input"
                        />
                    </div>
                    <div className={'proxy-list-wrapper'}>
                        {props.account.trial ?
                            <div>
                            <p
                              className={'proxy-list-name-list'}>
                                {localeService.getMessage('proxiesListLocation')}
                            </p>
                            <ProxyList
                              proxies={props.proxies}
                              favorites={props.favorites}
                              onSelect={commonActions.selectProxy}
                              account={props.account}
                              onChangeFavorite={commonActions.selectFavorite}
                              isTrial={true}
                            />
                            <ProxiesTrialBanner/>
                        </div>
                        : null
                        }

                        { props.favorites.length && !props.account.trial ?
                          <div>
                              <p className={'proxy-list-name-list'}>
                                  {localeService.getMessage('proxiesListFavourites')}
                              </p>
                              <ProxyList
                                proxies={props.proxies}
                                favorites={props.favorites}
                                onSelect={commonActions.selectProxy}
                                account={props.account}
                                onChangeFavorite={commonActions.selectFavorite}
                                isFavorites={true}
                              />
                          </div>
                          : null

                        }
                        <p className={'proxy-list-name-list'}>
                            {props.account.trial ?
                              localeService.getMessage('proxiesListPremium') :
                              localeService.getMessage('proxiesListLocation')}
                        </p>
                        <ProxyList
                          proxies={props.proxies}
                          favorites={props.favorites}
                          onSelect={commonActions.selectProxy}
                          account={props.account}
                          onChangeFavorite={commonActions.selectFavorite}
                        />
                    </div>
                </div>
                {/*<Divider />*/}
                {/*<ProxiesFooter*/}
                {/*    onHelpClick={() => openTab(HELP_CENTER_URL)}*/}
                {/*    onPremiumClick={navActions.openActivation}*/}
                {/*/>*/}
            </Page>
        );
    }

    onFilterChange(e) {
        const { props } = this;
        const value = e.target.value;

        props.commonActions.changeLocationsFilter(value);
    }
}


export default Proxies;
