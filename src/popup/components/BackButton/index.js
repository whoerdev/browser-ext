
import React/*, { Component }*/ from 'react';
import BackImage from '../../images/back-button.svg';
import CloseImage from '../../images/close-button.svg';
import CloseImageBlack from '../../images/close-button-black.svg';
// import Localize from '../../containers/Localize';
import './styles.css';

const BackButton = (props) => {
  const close = props.colorBlack ? CloseImageBlack : CloseImage;
  return (
      <div className="back-button" onClick={props.onClick}>
          {/*<div style={{lineHeight: '0px'}}>*/}
              <img
                src={props.close ? close : BackImage}
                className="back-image"
                alt=""/>
          {/*</div>*/}
          {/*<div className="back-text" onClick={props.onClick}>*/}
          {/*    <Localize value="backLabel"/>*/}
          {/*</div>*/}
      </div>
  );
};

export default BackButton;
