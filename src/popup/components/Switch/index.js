
import React, { Component } from 'react';
import './styles.css';


class Switch extends Component {

    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
    }

    render() {
        const { checked } = this.props;

        return (
            <label className="switch">
                <input type="checkbox" checked={checked} onChange={this.onChange}/>
                <span className="slider round">
                </span>
            </label>
        );
    }

    onChange(e) {
        const { onChange } = this.props;

        if (onChange) {
            onChange(e.target.checked);
        }
    }
}

export default Switch;
