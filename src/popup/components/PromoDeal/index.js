
import React from 'react';
// import Localize from '../../containers/Localize';
import localeService from '../../../services/locale';
// import HelpCenterSVG from '../../images/shape-8.svg';
// import './style.css';
import BackButton from "../BackButton";


const PromoDeal = (props) => {
  // const {account} = props;
  const testImage = 'https://whoer.net/images/whoer_email/banner_cybersummer.png';

  return(
    <div className={'promo-deal'}
      style={{
        position: 'absolute',
        height: '100%',
        width: '100%',
        top: 0,
        left: 0,
        'z-index': 99
      }}
    >
      <BackButton
        close={true}
        promo={true}
      />
      <a
        href={localeService.getLinkWithPrefix('vpn/buy')}
        target='_blank'
        rel='noopener noreferrer'
        style={{
          height: '100%',
          width: '100%',
          display: 'block',
          background: `url(${testImage}) no-repeat, center`,
          'background-size': 'cover'
        }}
      >
      </a>
    </div>
  );
};


export default PromoDeal;
