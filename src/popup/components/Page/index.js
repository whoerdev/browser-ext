
import React, { Component } from 'react';
// import Switch from '../Switch';
// import Divider from '../Divider';
import BackButton from '../BackButton';
import Localize from  '../../containers/Localize';
import optionsImage from '../../images/settings-img.svg';
import './styles.css';
import Logo from '../../images/logo.svg';


//
// const ToggleProxy = (props) => (
//     <div className="toggle">
//         <div className="toggler-status">
//             {props.enabled ? <Localize value="switchOnLabel"/> : <Localize value="switchOffLabel"/>}
//         </div>
//         <Switch checked={props.enabled} onChange={props.onToggle}/>
//     </div>
// );

class Page extends Component {

    constructor(props) {
        super(props);

        this.onOptionsClick = this.onOptionsClick.bind(this);
    }

    render() {
        const { title, children } = this.props;
        const { testState,backPage, optionsAvailable, needBackButton, needLogo, isClose, isCloseColorBlack } = this.props;
        const classNames = ['app'];
        let className = this.props.className || '';

        console.log(testState);

        if (className) {
            classNames.push(className);
        }

        // if (window.devicePixelRatio > 1) {
        //     classNames.push('retina');
        // }

        return (
            <div className={classNames.join(' ')}>
                <div className="header">
                    {needLogo ? this.renderLogo() : null}
                    {needBackButton ? <BackButton onClick={backPage} close={isClose}/> : null}
                    <div className="header-label"><Localize value={title || 'defaultPageTitle'}/></div>
                    {/*{proxyAvailable ? <ToggleProxy enabled={proxyEnabled} onToggle={onProxyToggle}/> : null}*/}
                    {optionsAvailable ? this.renderOptionsButton() : null}
                    {isClose && isCloseColorBlack ? <BackButton onClick={backPage} close={isClose} colorBlack={isCloseColorBlack}/>
                        : isClose ? <BackButton onClick={backPage} close={isClose}/> : null}
                </div>
                {/*<Divider/>*/}
                {children}
            </div>
        );
    }

    renderLogo() {
        return (
          <img
            className="header-logo"
            src={Logo}
            alt="logo"
          />
        )
    }

    renderOptionsButton() {
        return (
            <a
                className="page-header-options-button"
                onClick={this.onOptionsClick}
                tabIndex="-1"
            >
                <img src={optionsImage} alt=""/>
            </a>
        );
    }

    renderBackButton() {
        return (
          BackButton
        )
    }

    onOptionsClick(e) {
        const { onOptionsClick } = this.props;

        e.preventDefault();
        if (onOptionsClick) onOptionsClick();
    }
}

Page.defaultProps = {
    optionsAvailable: true,
    needBackButton: false,
    needLogo: false,
    isClose: false
};

export default Page;
