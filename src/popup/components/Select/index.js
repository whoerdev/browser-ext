
import React, { Component } from 'react';
import './styles.css';


class Select extends Component {

    constructor(props) {
        super(props);

        this.state = {
            open: false
        };

        this.onControlClick = this.onControlClick.bind(this);
        this.renderOption = this.renderOption.bind(this);
        this.docListener = this.docListener.bind(this);
    }

    componentDidMount() {
        document.addEventListener('click', this.docListener);
    }

    componentWillUnmount() {
        document.removeEventListener('click', this.docListener);
    }

    render() {
        const { state, props } = this;
        const selectClasses = ['sml-select'];

        if (state.open) {
            selectClasses.push('sml-select--open');
        }

        return (
            <div className={selectClasses.join(' ')}>
                <div className="sml-select-control" onClick={this.onControlClick}>
                    {this.renderValue()}
                    <div className="sml-select-toggle">
                        <div className="sml-select-arrow"/>
                    </div>
                </div>
                <div className="sml-select-list">
                    {props.items.map(this.renderOption)}
                </div>
            </div>
        );
    }

    renderValue() {
        const { props } = this;
        const { items, valueKey, labelKey, valueContent } = props;
        const render = valueContent || defaultRender;
        const item = items.find(item => item[valueKey] === props.value);

        return (
            <div className="sml-select-value">
                {item ? render(item) : null}
            </div>
        );

        function defaultRender(item) {
            return (
                <span>
                    {item[labelKey]}
                </span>
            );
        }
    }

    renderOption(item) {
        const { props } = this;
        const { valueKey, labelKey, optionContent } = props;
        const render = optionContent || defaultRender;

        return (
            <div
                className="sml-select-option"
                key={item[valueKey]}
                onClick={() => this.changeValue(item[valueKey])}
            >
               {render(item)}
            </div>
        );

        function defaultRender(item) {
            return (
                <span>
                    {item[labelKey]}
                </span>
            );
        }
    }

    onControlClick() {
        this.setState(state => ({open: !state.open}));
    }

    changeValue(value) {
        const { props } = this;

        props.onChange(value);
        this.setState({open: false});
    }

    docListener(event) {
        if (!event.target.closest('.sml-select')) {
            this.setState({open: false});
        }
    }
}

Select.defaultProps = {
    valueKey: 'value',
    labelKey: 'label'
};

export default Select;
