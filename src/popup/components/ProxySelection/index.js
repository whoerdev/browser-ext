
import React, { Component } from 'react';
import Localize from '../../containers/Localize';
import './style.css';
// import staryellow from "../../images/star-yellow.svg";
import star from "../../images/star.svg";
// import time from "../../images/time.svg";

const BACKEND_BASE_URL = process.env.WHOER_BACKEND_BASE_URL;


class ContentWithLocation extends Component {

    constructor(props) {
        super(props);

        this.state = {
            flagImage: ''
        };

        this.onClick = this.onClick.bind(this);
    }

    componentDidMount() {
        const { proxy } = this.props;

      // import('../../images/flags/' + `${proxy.countryCode}.png`).then(image => {
      //   this.setState({flagImage: image});
      // }).catch(err => {
      //     this.setState({flagImage: `${BACKEND_BASE_URL}/images/flagsvpn/${proxy.countryCode}.png`});
          this.setState({flagImage: `${BACKEND_BASE_URL}/images/map-pins/pin_${proxy.countryCode}.svg`});
      // });
    }

    render() {
        const { proxy, account } = this.props;
        const { flagImage } = this.state;
        let proxyStatus;
        let proxyStatusImg;
        if (account && !proxy.trial && !account.trial) {
            proxyStatusImg = star;
            proxyStatus = 'locationsItemPremium';
        } else {
            // proxyStatusImg = time;
            proxyStatus = 'locationsItemTrial';
        }
        const currentClass = ['current-location'];
        if (proxy.trial) {
            currentClass.push('current-location--trial');
        }

        return(
            <div className={currentClass.join(' ')} onClick={this.onClick}>
                <img src={flagImage} className="current-location__flag" alt=""/>
                <p className={'current-location__wrapper'}>
                    <span className="current-location__name">{proxy.countryName}</span>

                    {/*<a className="current-location__change">*/}
                    {/*  <Localize value="locationChangeLink"/>*/}
                    {/*</a>*/}
                </p>

                <span className="current-location__status">
                    <Localize value={proxyStatus}/>
                </span>
                {proxyStatusImg ? <img className="current-location__status-img" src={proxyStatusImg} alt="star"/> : null}
            </div>
            // <div className="current-location" onClick={this.onClick}>
            //     <div style={{lineHeight: '0'}}>
            //         <img src={flagImage} className="current-location-flag" alt=""/>
            //     </div>
            //
            //     <div className="country-name">{proxy.countryName}</div>
            //     <div className="wrapper-link-to-change-location">
            //         <a className="link-to-change-location">
            //             <Localize value="locationChangeLink"/>
            //         </a>
            //     </div>
            // </div>
        )
    }

    onClick(e) {
        const { props } = this;

        e.preventDefault();
        if (props.onClick) props.onClick();
    }
}



export default ContentWithLocation;