import React, {Component} from "react";
import localeService from "../../../services/locale";
import './style.css';
import Icon from '../../images/notification_icon.svg';

class RegistrationNotification extends Component {

  render() {
    // const { props } = this;
    const { navActions } = this.props;

    return (
      <div className="registration-notification">
        <img
          className={'registration-notification__icon'}
          height={70}
          src={Icon}
          alt={'notification logo'}
        />
        <h2 className={'registration-notification__title'}>{localeService.getMessage('registrationNotificationTitle')}</h2>
        <p className={'registration-notification__description'}>{localeService.getMessage('registrationNotificationDescription')}</p>
        <button
          className={'registration-notification__button'}
          onClick={navActions.openLogin}
        >
          {localeService.getMessage('registrationNotificationButton')}
        </button>
      </div>
    );
  }
}

export default RegistrationNotification