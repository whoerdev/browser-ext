
import React, { Component } from 'react';
import './style.css';
import Preloader from "../../images/three-dots.svg";


class Switch extends Component {

  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  render() {
    const { checked, ipInfo } = this.props;

    return (
        <div className="switch-wrapper">
            {ipInfo ?
                <div className="switch-wrapper__block">
                    <input id="bigSwitcher" type="checkbox" checked={checked} onChange={this.onChange}/>
                    <label htmlFor="bigSwitcher" className="switch-big">
                      <span className="slider-big round-big"></span>
                    </label>
                </div>
            :
                <img alt={''} src={Preloader} width={60} height={19}/>
            }
        </div>
    );
  }

  onChange(e) {
    const { onChange } = this.props;

    if (onChange) {
      onChange(e.target.checked);
    }
  }
}

export default Switch;
