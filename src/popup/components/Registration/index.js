import React, {Component} from "react";
import localeService from "../../../services/locale";
import './style.css';
import StartPage from "../StartPage";
// import {openLogin} from "../../actions/navigation";
import ovalPreloader from '../../images/oval-preloader.svg';

class Registration extends Component{

  constructor(props) {
    super(props);
    // this.state = {
    //   email: '',
    // }
    this.onEmailChange = this.onEmailChange.bind(this);
    this.onEmailSubmit = this.onEmailSubmit.bind(this);
  }
  render() {
    const { registration } = this.props;
    const {navActions, commonActions} = this.props;

    return (
      <StartPage>
        <h2 className="start-page-greeting">{localeService.getMessage('registrationTitle')}</h2>
        <p className="start-page-free-desc">{localeService.getMessage('registrationDescription')}</p>
        <form
          className="start-page-form"
          onSubmit={this.onEmailSubmit}
        >
          <input
            type="text"
            className="start-page-input"
            name=""
            placeholder={localeService.getMessage('inputEmail')}
            onChange={this.onEmailChange}
            onClick={commonActions.resetError}
            value={registration.email}
          />
          {
            registration.error ? <p className="start-page-error start-page-error--email">{registration.error.message}</p> : null
          }
          <label className={'start-page-label'}>
            { registration.preloader ?

              <img alt={''} className={'start-page-preloader'} src={ovalPreloader}/>
              :
              <input
              type="submit"
              className='start-page-submit'
              value={localeService.getMessage('registrationButton')}
              />
            }
          </label>
        </form>
        <p className="start-page-hint">
          {localeService.getMessage('registrationHint')}&nbsp;
            <span onClick={navActions.openLogin}>
              {localeService.getMessage('registrationHintLink')}
            </span>
        </p>
      </StartPage>
    )
  }
  onEmailChange(e) {
    const { commonActions } = this.props;
    const value = e.target.value;
    commonActions.changeEmail(value)
  }

  onEmailSubmit(e) {
    const { commonActions } = this.props;
    // const { navActions, registration } = this.props;
    e.preventDefault();
    commonActions.registerTrial();
    // navActions.openRegistrationNotification();
  }
}
export default Registration