
import React/*, { Component }*/ from 'react';
import Localize from '../../containers/Localize';
import LocationImage from '../../images/shape-7.svg';

const ContentWithoutLocation = props => {
    return (
        <div className="location-suggestion-content" onClick={onClick}>
            <div style={{lineHeight: 0}}>
                <img src={LocationImage} className="location-image" alt=""/>
            </div>
            <a className="link-to-virtual-locations link-text">
                <Localize value="locationSelectLink"/>
            </a>
        </div>
    );

    function onClick(e) {
        e.preventDefault();
        if (props.onClick) props.onClick();
    }
};

export default ContentWithoutLocation;