
import React, { Component } from 'react';
import ProxyListItem from '../ProxyListItem';


class ProxyListFavorites extends Component {

  render() {
    const { props } = this;


    return (
      <ul className="proxy-list">
        {props.proxies.filter(proxy => props.favorites.includes(proxy.countryCode)).map(proxy => {
          console.log(proxy);
          return (
            <ProxyListItem
              key={proxy.countryCode}
              proxy={proxy}
              onSelect={props.onSelect}
              account={props.account}
              onChangeFavorite={props.onChangeFavorite}
              favorites={props.favorites}
            />
          )
        })}
      </ul>
    );
  }
}


export default ProxyListFavorites;
