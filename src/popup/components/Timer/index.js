
import React, { Component } from 'react';
import Page from '../../containers/Page';
import CircularProgressbar from 'react-circular-progressbar';
import Localize from '../../containers/Localize'
import 'react-circular-progressbar/dist/styles.css';
import './styles.css';

const REGISTRATION_DELAY = process.env.WHOER_REGISTRATION_DELAY;


class Timer extends Component {

    render() {
        const { delay } = this.props;
        const percent = delay < REGISTRATION_DELAY ? Math.round(100 - delay / REGISTRATION_DELAY * 100) : 0;

        return (
            <Page className="reg-timer">
                <CircularProgressbar
                    className="reg-timer__progress reg-timer-progress"
                    percentage={percent}
                    textForPercentage={() => Math.round(delay / 1000)}
                    strokeWidth={6}
                />
                <div className="reg-timer__caption"><Localize value="timerDescriptionTitle"/></div>
                <div className="reg-timer__detail"><Localize value="timerDescriptionText"/></div>
            </Page>
        );
    }
}

export default Timer;
