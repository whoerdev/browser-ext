import React, {Component} from "react";
import localeService from "../../../services/locale";
import './style.css';
import unlock from '../../images/unlock.svg';

class ProxiesTrialBanner extends Component {

  render() {
    // const { props } = this;
    // const { navActions } = this.props;

    return (
      <a
        className="proxies-trial-banner"
        href='https://whoer.net/vpn/buy'
        target='_blank'
        rel='noopener noreferrer'
      >
        <img
          src={unlock}
          alt=""
          className={'proxies-trial-banner__icon'}
        />
        <p className={'proxies-trial-banner__text'}>
          {localeService.getMessage('proxiesTrialBanner')}
        </p>
      </a>
    );
  }
}

export default ProxiesTrialBanner