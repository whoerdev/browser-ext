
import React, { Component } from 'react';
import Page from '../../containers/Page';
import BackButtonGo from '../BackButtonGo';
import Localize from '../../containers/Localize';
import RetryImage from '../../images/retry-white.svg';
import './styles.css';

class Error extends Component {

    render() {
        const { props } = this;

        return (
            <Page>
                <div className="error-box">
                    <div style={{lineHeight: 0}}>
                        <img src={this.props.image} className="warning-image" alt=""/>
                    </div>
                    <div className="error-status">
                        {this.props.title}
                    </div>
                    <div className="error-text">
                        {this.props.message}
                    </div>
                </div>
                <div className="error-footer">
                    {props.onRetry ? this.renderRetry() : null}
                    {props.onBack ? <BackButtonGo onClick={props.onBack}/> : null}
                </div>
            </Page>
        );
    }

    renderRetry() {
        const { props } = this;

        return (
            <div className="retry-button">
                <div>
                    <img src={RetryImage} className="retry-image" alt=""/>
                </div>
                <div className="retry-text" onClick={props.onRetry}>
                    <Localize value="retryLabel"/>
                </div>
            </div>
        );
    }
}

export default Error;
