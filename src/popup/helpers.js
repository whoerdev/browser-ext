import {CodeExpiredError, CodeInvalidError} from "../errors/api";

export function collectAccount(state) {
    const { trial, premium } = state;

    if (premium && premium.enabled) return premium;
    if (trial) return trial;
    return null;
}

export function isCodeError(err) {
    return err instanceof CodeInvalidError || err instanceof CodeExpiredError;
}