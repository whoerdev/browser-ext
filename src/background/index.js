import storageService from '../services/storage';
import messageService from '../services/message';
import requestService/*, {onBeforeRequest}*/ from '../services/request';
import privacyService from '../services/privacy';
import proxyService from '../services/proxy';
import Account from '../models/account';
import Proxy from '../models/proxy';
import axios from "axios/index";

const CONNECT_CHECK_URL = process.env.WHOER_CONNECT_CHECK_URL;
const CONNECT_CHECK_TIMEOUT = +process.env.WHOER_CONNECT_CHECK_TIMEOUT;
const REGISTRATION_DELAY = process.env.WHOER_REGISTRATION_DELAY;
const PREVENT_WEBRTC_LEAK_DEFAULT = process.env.WHOER_PREVENT_WEBRTC_LEAK_DEFAULT === 'true';
const DISABLE_FLASH_DEFAULT = process.env.WHOER_DISABLE_FLASH_DEFAULT === 'true';
const ADJUST_TIMEZONE_DEFAULT = process.env.WHOER_ADJUST_TIMEZONE_DEFAULT === 'true';

const defaultOptions = {
    preventWebRTCLeak: PREVENT_WEBRTC_LEAK_DEFAULT,
    disableFlash: DISABLE_FLASH_DEFAULT,
    adjustTimezone: ADJUST_TIMEZONE_DEFAULT
};

const pendingRequests = [];

let proxyChecked;
let currentProxy;
let storageProxy;
let storageTrial;
let storagePremium;
let storageOptions;
let intervalId;
// let intervalCheckId;

requestService.onBeforeRequest.addListener(
  beforeRequestHandler,
  {urls: ["<all_urls>"]},
  ['blocking']
);

// requestService.onBeforeSendHeaders.addListener(
//     beforeSendHeadersHandler,
//     {urls: ["<all_urls>"]},
//     ['blocking', 'requestHeaders']
// );

requestService.onAuthRequired.addListener(
  provideCredentials,
  {urls: ["<all_urls>"]},
  ['blocking']
);

requestService.onCompleted.addListener(
  completeRequest,
  {urls: ["<all_urls>"]}
);

requestService.onErrorOccurred.addListener(
  completeRequest,
  {urls: ["<all_urls>"]}
);

storageService.getItems().then(items => {

    storageService.addChangeListener((changes, areaName) => {
        const { proxy, trial, premium, options } = changes;

        if (areaName !== 'local') {
            return;
        }

        if (proxy) proxyChecked = false;
        if (proxy) storageProxy = Proxy.validate(proxy.newValue) ? proxy.newValue : null;
        if (trial) storageTrial = Account.validate(trial.newValue) ? trial.newValue : null;
        if (premium) storagePremium = Account.validate(premium.newValue) ? premium.newValue : null;
        if (options) initOptions(options.newValue);

        checkStatus();
    });

    storageProxy = Proxy.validate(items.proxy) ? items.proxy : null;
    storageTrial = Account.validate(items.trial) ? items.trial : null;
    storagePremium = Account.validate(items.premium) ? items.premium : null;
    initOptions(items.options);

    checkStatus();
});

messageService.addCustomListener('checkStatus', () => {
    checkStatus();
});

function checkStatus() {
    if (!intervalId) {
        intervalId = setInterval(checkStatus, 1000);
    }

    const acct = storagePremium && storagePremium.enabled ? storagePremium : storageTrial;
    const delay = countRegistrationDelay((acct && acct.created) || Date.now());

    messageService.sendCustomMessage('registrationDelay', delay);

    if (!delay) {
        clearInterval(intervalId);
        intervalId = null;
    }

    if (delay || !storageProxy || !storageProxy.enabled) {
        removeProxy().catch(err => {
            console.error('Proxy remove failed\n', err);
        });
    }

    if (!delay && storageProxy && storageProxy.enabled) {
        setProxy(storageProxy).then(proxy => {
            if (!proxyChecked) checkProxy(proxy);
        }).catch(err => {
            console.error('Proxy set failed\n', err);
        });
    }
}

function countRegistrationDelay(from) {
    const passed = Date.now() - from;
    return passed < REGISTRATION_DELAY ? REGISTRATION_DELAY - passed : 0;
}

const browser = window.browser || window.chrome;

function setProxy(data) {
    currentProxy = data;
    browser.browserAction.setIcon({ path: "con@.png" });
    return proxyService.setProxy(data).catch(err => {
        browser.browserAction.setIcon({ path: "off.png" });
    }).then(() => {
        browser.browserAction.setIcon({ path: "on.png" });
        return data
    });
}

function getIconOff() {
    let canvas = document.createElement("canvas");
    let ctx = canvas.getContext("2d");
    let w = [3,3,3,4,4,4,4,6,4.5,6,4.5,7,5,7,5,8,5.5,8,5.5,9.5,6,9.5,6,9,6.5,9,6.5,8,7,8,7,7,7.5,7,7.5,5.5,8,5.5,8,4.5,8.5,4.5,8.5,3,9.5,3,9.5,4.5,10,4.5,10,5.5,10.5,5.5,10.5,7,11,7,11,8,11.5,8,11.5,9,12,9,12,9.5,12.5,9.5,12.5,8.5,13,8.5,13,7.5,13.5,7.5,13.5,6.5,16.5,6.5,16.5,7,16,7,16,8,15.5,8,15.5,9,15,9,15,10,14.5,10,14.5,11,14,11,14,12,13.5,12,13.5,13,13,13,13,14,12.5,14,12.5,15,12,15,12,14.5,11.5,14.5,11.5,13.5,11,13.5,11,12.5,10.5,12.5,10.5,11.5,10,11.5,10,10.5,9.5,10.5,9.5,9.5,8.5,9.5,8.5,10.5,8,10.5,8,11.5,7.5,11.5,7.5,12.5,7,12.5,7,13.5,6.5,13.5,6.5,14.5,6,14.5,6,15,5.5,15,5.5,14,5,14,5,13,4.5,13,4.5,12,4,12,4,11,3.5,11,3.5,10,3,10,3,9,2.5,9,2.5,8,2,8,2,7,1.5,7,1.5,6,1,6,1,5,0.5,5,0.5,4,0,4];
    let w2 = [18,3.5, 17.5,3.5, 17.5,4.5, 17,4.5, 17,5.5, 14,5.5, 14,5, 14.5,5, 14.5,4, 15,4, 15,3];
    ctx.beginPath();ctx.moveTo(0,3);
    for(let index=0; index < w.length; index+=2) {
        ctx.lineTo(w[index],w[index+1]);
    }
    ctx.fillStyle = '#737373';ctx.fill();ctx.closePath();
    ctx.beginPath(); ctx.moveTo(18,3);
    for(let index=0; index < w2.length; index+=2) {
        ctx.lineTo(w2[index],w2[index+1]);
    }
    ctx.fillStyle = '#737373';ctx.fill();ctx.closePath();

    return ctx.getImageData(0, 0, 18, 18);
}

function removeProxy() {
    return proxyService.removeProxy().then(() => {
        browser.browserAction.setIcon({ imageData: getIconOff() });
        currentProxy = null;
    });
}

//show Subscription for freeUser's first open browser
browser.runtime.onStartup.addListener(handleStartup);
function handleStartup() {
    storageService.setItem('firstStartBrowser', {firstStartBrowser: true});
}

function checkProxy(proxy) {
    console.log('Check proxy', proxy);
    return axios.get(CONNECT_CHECK_URL, {timeout: CONNECT_CHECK_TIMEOUT}).catch(err => {
        console.log('Check proxy failed', proxy === storageProxy, proxy);
        if (proxy === storageProxy) {
            messageService.sendCustomMessage('proxyError', {proxy});
            setProxy(proxy);
        }
    }).then(() => {
        console.log('Check proxy done', proxy === storageProxy, proxy);
        if (proxy === storageProxy) {
            proxyChecked = true;
        }
    });
}

function provideCredentials(details) {
    const proxy = currentProxy;
    const { isProxy, requestId } = details;

    if (!isProxy) {
        console.log('> not_proxy', requestId, !!proxy, pendingRequests.indexOf(requestId));
        return {
            cancel: true
        };
    }

    if (!proxy || pendingRequests.indexOf(requestId) !== -1) {
        console.log('> block_proxy', requestId, !!proxy, pendingRequests.indexOf(requestId));
        return {
            cancel: true
        };
    }

    console.log('> in_proxy', requestId, !!proxy, pendingRequests.indexOf(requestId));

    pendingRequests.push(requestId);

    return {
        authCredentials: {
            username: proxy.username,
            password: proxy.password
        }
    };
}

function completeRequest(details) {
    const { requestId } = details;
    const index = pendingRequests.indexOf(requestId);

    if (index > -1) {
        pendingRequests.splice(index, 1);
    }
}

function beforeRequestHandler(details) {
    const { requestId } = details;
    if (storageOptions && storageOptions.disableFlash && /\.swf$/i.test(details.url)) {
        console.log('> block_request', requestId, !!currentProxy, pendingRequests.indexOf(requestId));

        return {
            cancel: true
        };
    }

    // console.log('> pass_request', requestId, !!currentProxy, pendingRequests.indexOf(requestId));
}

//function beforeSendHeadersHandler(details) {
//    const { requestHeaders } = details;
//    const TIMEZONE_HEADER = 'Time-Zone';

//    if (storageOptions && storageOptions.adjustTimezone) {
//        let timezoneHeader = requestHeaders.find(isTimezone);
//        let offset = Math.round((new Date).getTimezoneOffset() / 60);

//        if (!timezoneHeader) {
//            timezoneHeader = {name: TIMEZONE_HEADER};
//            requestHeaders.push(timezoneHeader);
//        }

//        timezoneHeader.value = formatTimezone(offset);
//        return {requestHeaders}
//    }

//    function formatTimezone(offset) {
//        let prefix = '';

//        if (!offset) {
//            return '0000';
//        }

//        if (offset > 0) {
//            prefix += '+';
//        }

//        if (offset < 0) {
//            prefix += '-';
//            offset *= -1;
//        }

//        if (offset < 10) {
//            prefix += '0';
//        }

//        return prefix + offset + '00';
//    }

//    function isTimezone(header) {
//        return header.name.toLowerCase() === TIMEZONE_HEADER.toLowerCase();
//    }
//}

function initOptions(options) {
    storageOptions = Object.assign({}, defaultOptions, options);

    const { preventWebRTCLeak } = storageOptions;
    const webRTCIPHandlingPolicy = preventWebRTCLeak !== false ? 'disable_non_proxied_udp' : 'default';
    //ошибка в коде  "disable_non_proxied_udp"
    privacyService.setWebRTCIPHandlingPolicy(webRTCIPHandlingPolicy);
}